import { USER } from './const'

const initState = {}

export default function countriesReducer(state = initState, action) {
	switch (action.type) {
		case USER.SET:
			return action.payload
		case USER.UNSET:
			return { ...initState }
		default:
			return state
	}
}