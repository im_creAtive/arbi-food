import { RESTAURANT } from './const'

const initState = {}

export default function restaurantReducer(state = initState, action) {
  switch (action.type) {
    case RESTAURANT.SET:
      return action.payload
    default:
      return state
  }
}