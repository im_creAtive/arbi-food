import { CATEGORIES } from './const'

const initState = {
  list: []
}

export default function categoriesReducer(state = initState, action) {
  switch (action.type) {
    case CATEGORIES.SET:
      return { list: action.payload }
    default:
      return state
  }
}