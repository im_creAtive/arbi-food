import { routerReducer } from 'react-router-redux'
import { combineReducers } from 'redux'
import { langReducer } from 'react-redux-multilang'

import env from './env'
import user from './user'
import users from './users'
import restaurant from './restaurant'
import categories from './categories'
import dishes from './dishes'

export default combineReducers({
    router: routerReducer,
    lang: langReducer,
    env,
    user,
    users,
    restaurant,
    categories,
    dishes
})