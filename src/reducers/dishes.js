import { DISHES } from './const'

const initState = {
  list: []
}

export default function dishesReducer(state = initState, action) {
  switch (action.type) {
    case DISHES.SET:
      return { list: action.payload }
    default:
      return state
  }
}