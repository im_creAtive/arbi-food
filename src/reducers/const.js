export const ENV = {
    SHOW: 'ENV_LOADER_SHOW',
    HIDE: 'ENV_LOADER_HIDE'
}

export const USER = {
    SET: 'USER_SET',
    UNSET: 'USER_UNSET'
}

export const USERS = {
    SET: 'USERS_SET'
}

export const RESTAURANT = {
    SET: 'RESTAURANT_SET'
}

export const CATEGORIES = {
    SET: 'CATEGORIES_SET'
}

export const DISHES = {
    SET: 'DISHES_SET'
}