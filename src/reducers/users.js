import { USERS } from './const'

const initState = {
  list: []
}

export default function usersReducer(state = initState, action) {
  switch (action.type) {
    case USERS.SET:
      return { list: action.payload }
    default:
      return state
  }
}