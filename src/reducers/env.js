import { ENV } from './const'

const initState = {
  count: 0,
  total: 0
}

export default function countriesReducer(state = initState, action) {
  switch (action.type) {
    case ENV.SHOW:
      return { count: state.count + 1, total: state.total + 1 }
    case ENV.HIDE:
      let count = state.count - 1;
      return { count, total: count > 0 ? state.total : 0  }
    default:
      return state
  }
}