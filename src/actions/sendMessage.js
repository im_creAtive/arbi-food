import Api from './api'

const endpoint = 'SendMessage'

export async function createMail(email, mail) {
  Api.showLoader()
  await Api.post(`${endpoint}/CreateMail?email=${email}`, mail);
  Api.hideLoader()

  //let data = await response.json()
  //if (data.Message) throw new Error(data.Message)

  //return data;
}
