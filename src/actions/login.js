import Api from './api'
import { store } from '../index'
import { push } from 'react-router-redux'
import { USER } from '../reducers/const';

const endpoint = 'login'

export async function token(login, password) {
  const headers =  {
    'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'
  }

  Api.showLoader()
  let response = await Api.post(endpoint + '/token', 'grant_type=password&userName=' + login + '&password=' + password, false, headers, false);
  Api.hideLoader()

  let data = await response.json()
  if (data.error_description) throw new Error(data.error_description);
  
  Api.setTokenToLocalStorage(data.access_token)
}

export async function user() {
  Api.showLoader()
  let response = await Api.get(endpoint + '/user');
  Api.hideLoader()

  let data = await response.json()
  if(data.Message) throw new Error(data.Message)

  return user;
}

export function checkAuth(admin = false) {
  const state = store.getState();

  if(!state.user.Id) return false

  if (!state.user.IsActive || !state.user.IsActive) return false;

  if (!admin) return true;
  else {
    return state.user.RoleIds.indexOf('Admin') > -1
  }
}

export function logout() {
  Api.deleteTokenFromLocalStorage()
  store.dispatch({ type: USER.UNSET })
  store.dispatch(push('/login'))
}