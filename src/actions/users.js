import Api from './api'

import { store } from '../index'
import { USER, USERS } from '../reducers/const'

const endpoint = 'users'

export async function add(user) {
  Api.showLoader()
  let response = await Api.post(endpoint, user);
  Api.hideLoader()

  let data = await response.json()
  if (data.Message) throw new Error(data.Message)

  return data;
}

export async function currentUser() {
  Api.showLoader()
  let response = await Api.get(endpoint + '/CurrentUser');
  Api.hideLoader()

  let data = await response.json()
  if (data.Message) throw new Error(data.Message)

  store.dispatch({ type: USER.SET, payload: data })

  return data;
}

export async function get() {
  Api.showLoader()
  let response = await Api.get(endpoint);
  Api.hideLoader()

  let data = await response.json()
  if(data.Message) throw new Error(data.Message)

  store.dispatch({ type: USERS.SET, payload: data })
}

export async function del(id) {
  Api.showLoader()
  let response = await Api.delete(endpoint + '?userId=' + id);
  Api.hideLoader()

  let data = await response.json()
  if (data.Message) throw new Error(data.Message)

  return data;
}

export async function edit(user) {
  Api.showLoader()
  let response = await Api.patch(endpoint + '/' + user.Id, user);
  Api.hideLoader()

  let data = await response.json()
  if (data.Message) throw new Error(data.Message)

  return data;
}