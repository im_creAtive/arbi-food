import Api from './api'


export function restaurantPhoto(params) {
  return Api.host + 'Files/Download' + params + '&type=Restaurant'
}

export function categoryPhoto(params) {
  return Api.host + 'Files/Download' + params + '&type=CategoriesOfDishes'
}

export function dishPhoto(params) {
  return Api.host + 'Files/Download' + params + '&type=Dish'
}
