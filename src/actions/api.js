import { ENV } from '../reducers/const'
import { store } from '../index'

export default class Api{
  
  static host = "https://www.foodstory.io:446/api/";
  // dev http://37.48.115.71:81/api/
  // prod https://www.foodstory.io:446/api/ 

  static getTokenFromLocalStorage(){
    return localStorage.getItem('access_token');
  }

  static setTokenToLocalStorage(token){
    return localStorage.setItem('access_token', token);
  }

  static deleteTokenFromLocalStorage(){
    return localStorage.removeItem('access_token');
  }

  static getSelectedUserIdFromLocalStorage(){
    return localStorage.getItem('Selected-User-Id')
  }

  static setSelectedUserIdToLocalStorage(userId){
    return localStorage.setItem('Selected-User-Id', userId);
  }

  static deleteSelectedUserIdFromLocalStorage(){
    return localStorage.removeItem('Selected-User-Id');
  }

  static authorization(headers){
    let selectedUserId = Api.getSelectedUserIdFromLocalStorage()

    headers.Authorization = 'Bearer ' + Api.getTokenFromLocalStorage()
    if(selectedUserId) headers['Selected-User-Id'] = selectedUserId

    return headers
  }

  static get(endpoint, authorization = true, id = ''){
    let headers = {

    }

    if(authorization) headers = Api.authorization(headers)

    return fetch(`${Api.host}${endpoint}`, {
      mode: 'cors',
      method: 'GET',
      headers
    })
  }

  static post(endpoint, body, authorization = true, headers = {}, convertToJson = true){
    if (convertToJson) body = JSON.stringify(body)

    if(Object.keys(headers).length === 0) headers = {
      'Content-Type': 'application/json',
      'Content-Length': body.length
    }

    if(authorization) headers = Api.authorization(headers)

    return fetch(`${Api.host}${endpoint}`, {
      mode: 'cors',
      method: 'POST',
      headers,
      body
    })
  }

  static put(endpoint, body, authorization = true, headers = {}){
    body = JSON.stringify(body)

    if(Object.keys(headers).length === 0) headers = {
      'Content-Type': 'application/json',
      'Content-Length': body.length
    }

    if(authorization) headers = Api.authorization(headers)

    return fetch(`${Api.host}${endpoint}`, {
      mode: 'cors',
      method: 'PUT',
      headers,
      body
    })
  }

  static patch(endpoint, body, authorization = true, headers = {}, convertToJson = true) {
    if (convertToJson) body = JSON.stringify(body)

    if (Object.keys(headers).length === 0) headers = {
      'Content-Type': 'application/json',
      'Content-Length': body.length
    }

    if(authorization) headers = Api.authorization(headers)

    return fetch(`${Api.host}${endpoint}`, {
      mode: 'cors',
      method: 'PATCH',
      headers,
      body
    })
  }

  static delete(endpoint, authorization = true) {
    let headers = {

    }

    if(authorization) headers = Api.authorization(headers)

    return fetch(`${Api.host}${endpoint}`, {
      mode: 'cors',
      method: 'DELETE',
      headers
    })
  }

  static showLoader(){
    store.dispatch({ type: ENV.SHOW });
  }

  static hideLoader(){
    store.dispatch({ type: ENV.HIDE });
  }
}