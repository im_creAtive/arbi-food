import Api from './api'

import { store } from '../index'
import { RESTAURANT } from '../reducers/const'

const endpoint = 'Restaurant'


export async function current() {
  Api.showLoader()
  let response = await Api.get(endpoint + '/Current');
  Api.hideLoader()

  let data = await response.json()
  if (data.Message) throw new Error(data.Message)

  store.dispatch({ type: RESTAURANT.SET, payload: data })

  return data;
}


export async function edit(restaurant) {
  Api.showLoader()
  let response = await Api.patch(`${endpoint}?id=${restaurant.Id}`, restaurant);
  Api.hideLoader()

  let data = await response.json()
  if (data.Message) throw new Error(data.Message)

  return data;
}

export async function upload(file, id) {
  const body = {
    Name: file.name,
    Body: await fileToBlob(file),
    Type: file.type
  }

  Api.showLoader()
  let response = await Api.post(`${endpoint}/Upload?id=${id}`, body);
  Api.hideLoader()

  let data = await response.json()
  if (data.Message) throw new Error(data.Message)

  return data
}

function fileToBlob(file) {
  return new Promise(async (resolve, reject) => {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result.split(",")[1])
    reader.onerror = error => reject(error)
  })
}