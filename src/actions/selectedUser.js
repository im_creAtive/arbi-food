import Api from './api'
import { store } from '../index'
import { push } from 'react-router-redux'

export function setSeletedUserId(userId){
  Api.setSelectedUserIdToLocalStorage(userId)
  store.dispatch(push('/login'))
}

export function removeSeletedUserId(){
  Api.deleteSelectedUserIdFromLocalStorage()
  store.dispatch(push('/login'))
}

export function getSelectedUserId(){
  return Api.getSelectedUserIdFromLocalStorage()
}