import Api from './api'

import { store } from '../index'
import { CATEGORIES } from '../reducers/const'

const endpoint = 'CategoriesOfDishes'

export async function add(idRestoraunt, category) {
  Api.showLoader()
  let response = await Api.post(endpoint + '?idRestoraunt=' + idRestoraunt, category);
  Api.hideLoader()

  let data = await response.json()
  if (data.Message) throw new Error(data.Message)

  return data;
}

export async function get(idRestaurant) {
  Api.showLoader()
  let response = await Api.get(endpoint + '?idRestaurant=' + idRestaurant);
  Api.hideLoader()

  let data = await response.json()

  store.dispatch({ type: CATEGORIES.SET, payload: data.CategoriesOfDishes })
}

export async function edit(category) {
  Api.showLoader()
  let response = await Api.patch(endpoint + '?idCategory=' + category.Id, category);
  Api.hideLoader()

  let data = await response.json()
  if (data.Message) throw new Error(data.Message)

  return data;
}

export async function upload(file, idCategory) {
  const body = {
    Name: file.name,
    Body: await fileToBlob(file),
    Type: file.type
  }

  Api.showLoader()
  let response = await Api.post(`${endpoint}/Upload?idCategory=${idCategory}`, body);
  Api.hideLoader()

  let data = await response.json()
  if (data.Message) throw new Error(data.Message)

  return data
}

export async function del(id) {
  Api.showLoader()
  let response = await Api.delete(endpoint + '/' + id);
  Api.hideLoader()

  if (response.status !== 200) throw new Error('Error, status: ' + response.status)
  let data = await response.json()

  return data
}

function fileToBlob(file) {
  return new Promise(async (resolve, reject) => {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result.split(",")[1])
    reader.onerror = error => reject(error)
  })
}