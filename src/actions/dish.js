import Api from './api'

import { store } from '../index'
import { DISHES } from '../reducers/const'

const endpoint = 'Dish'

export async function add(dish) {
  Api.showLoader()
  let response = await Api.post(endpoint, dish);
  Api.hideLoader()

  let data = await response.json()
  if (data.Message) throw new Error(data.Message)

  return data;
}

export async function get(idRestaurant) {
  Api.showLoader()
  let response = await Api.get(endpoint + '/GetAllFromRestaurant?idRestaurant=' + idRestaurant);
  Api.hideLoader()

  let data = await response.json()

  store.dispatch({ type: DISHES.SET, payload: data.Dishes })
}

export async function edit(dish) {
  Api.showLoader()
  let response = await Api.patch(endpoint + '?idDish=' + dish.Id, dish);
  Api.hideLoader()

  let data = await response.json()
  if (data.Message) throw new Error(data.Message)

  return data;
}

export async function uploadPhoto(file, idDish) {
  const body = {
    Name: file.name,
    Body: await fileToBlob(file),
    Type: file.type
  }

  Api.showLoader()
  let response = await Api.post(`${endpoint}/UploadPhoto?idDish=${idDish}`, body);
  Api.hideLoader()

  let data = await response.json()
  if (data.Message) throw new Error(data.Message)

  return data
}

export async function uploadFiles(files, idDish) {

  const body = await Promise.all(files.map(async (file) => {
    return {
      Name: file.name,
      Body: await fileToBlob(file),
      Type: file.type
    }
  }))

  console.log('files', body)

  Api.showLoader()
  let response = await Api.post(`${endpoint}/UploadFiles?idDish=${idDish}`, body);
  Api.hideLoader()

  console.log(response)

  let data = await response.json()
  if (data.Message) throw new Error(data.Message)

  return data
}

export async function del(id) {
  Api.showLoader()
  let response = await Api.delete(endpoint + '?dishId=' + id);
  Api.hideLoader()

  if (response.status !== 200) throw new Error('Error, status: ' + response.status)
  let data = await response.json()

  return data
}

function fileToBlob(file) {
  return new Promise(async (resolve, reject) => {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result.split(",")[1])
    reader.onerror = error => reject(error)
  })
}