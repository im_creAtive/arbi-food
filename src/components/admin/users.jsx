import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'
import { connect } from 'react-redux'
import dateFormat from 'dateformat'
import { toast } from 'react-toastify'

import Paper from 'material-ui/Paper'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table'
import Toggle from 'material-ui/Toggle'
import IconButton from 'material-ui/IconButton'

import UserDialog from './dialog'
import * as usersActions from '../../actions/users'
import * as seletedUserActions from '../../actions/selectedUser'

class Users extends Component {

  async componentDidMount(){
    try{
      await usersActions.get() 
    } catch(e) {
      toast.error(e.message || 'Unknown error')
    }
  }

  async onDelete(id){
    if (!window.confirm('Do you confirm the deletion?')) return;

    try{
      let response = await usersActions.del(id);
      await usersActions.get();

      console.log(response)
      toast.success('User successfully deleted')
    } catch(e) {
      console.log(e)
      toast.error(e.message)
    }
  }

  async onChangeStatus(user){
    try {
      user.IsActive = !user.IsActive;

      await usersActions.edit(user);
      usersActions.get()
    } catch (e) {
      console.log(e)
      toast.error(e.message)
    }
  }

  onSelectUserId(userId){
    seletedUserActions.setSeletedUserId(userId)
  }

  render() {
    const users = this.props.users.list.map((user, index) => {
      return (
        <TableRow key={index}>
          <TableRowColumn>{user.FirstName}</TableRowColumn>
          <TableRowColumn>{user.LastName}</TableRowColumn>
          <TableRowColumn>{user.Email}</TableRowColumn>
          <TableRowColumn>{user.PhoneNumber}</TableRowColumn>
          <TableRowColumn>
            <Toggle
              toggled={user.IsActive}
              onToggle={() => this.onChangeStatus(user)}
            />
          </TableRowColumn>
          <TableRowColumn>{dateFormat(user.LastLogged, 'yyyy-mm-dd HH:MM')}</TableRowColumn>
          <TableRowColumn style={{ width: 140 }}>
            <IconButton onClick={() => this.onSelectUserId(user.Id)}>
              <i className={css(styles.actions) + " material-icons"}>launch</i>
            </IconButton>
            <UserDialog user={user} />
            <IconButton onClick={() => this.onDelete(user.Id)}>
              <i className={css(styles.actions) + " material-icons"}>delete</i>
            </IconButton>
          </TableRowColumn>
        </TableRow>
      )
    })

    return (
      <div className={css(styles.wrapper)}>
        
        <Paper zDepth={1} className={css(styles.title)}>
          <span>Users</span>
          <UserDialog />
        </Paper>

        <Paper zDepth={1} style={{ marginTop: 15 }}>
          <Table selectable={false}>
            <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
              <TableRow>
                <TableHeaderColumn>First Name</TableHeaderColumn>
                <TableHeaderColumn>Last Name</TableHeaderColumn>
                <TableHeaderColumn>Email</TableHeaderColumn>
                <TableHeaderColumn>Phone number</TableHeaderColumn>
                <TableHeaderColumn>Is active</TableHeaderColumn>
                <TableHeaderColumn>Last logged</TableHeaderColumn>
                <TableHeaderColumn style={{ width: 140 }}>Actions</TableHeaderColumn>
              </TableRow>
            </TableHeader>
            <TableBody displayRowCheckbox={false}>
              {users}
            </TableBody>
          </Table>
        </Paper>

      </div>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    padding: 15
  },

  title: {
    padding: 10,
    display: 'flex',
    justifyContent: 'space-between',
    ':nth-child(1n) > span': {
      fontSize: 20,
      fontWeight: 'bold',
      padding: '6px 0',
      color: '#5f5f5f'
    }
  },

  table: {
    marginTop: 15
  },

  actions: {
    color: '#588b8b'
  }
})

export default connect(state => ({ user: state.user, users: state.users }))(Users)