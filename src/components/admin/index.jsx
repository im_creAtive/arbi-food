import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'
import { connect } from 'react-redux'
import { toast } from 'react-toastify'

import { NavLink } from 'react-router-dom'
import Avatar from 'material-ui/Avatar'

import icon from '../../sources/img/icon_alpha.png'

import Users from './users'

import * as loginActions from '../../actions/login'

class Admin extends Component {

  constructor(props){
    super(props);
    
    if (!loginActions.checkAuth(true)){
      toast.error('This section allowed only for admins.')
      this.props.history.push('/login')
    }
  }

  onLogout(e){
    e.preventDefault();

    loginActions.logout()
  }

  render() {
    return (
      <div className={css(styles.wrapper)}>
        <div className={css(styles.sidebar)}>
          <img className={css(styles.logo)} src={icon} alt="logo" />
          <ul className={css(styles.menu)}>
            <li><NavLink to="/admin/"><i className="material-icons">group</i> Users</NavLink></li>
            <li><a href="" onClick={e => this.onLogout(e)}><i className="material-icons">exit_to_app</i> Logout</a></li>
          </ul>
        </div>
        <div className={css(styles.main)}>
          <div className={css(styles.header)}>
            <Avatar size={30} className={css(styles.avatar)}>KB</Avatar>
            {this.props.user.Email}
          </div>
          <div className={css(styles.content)}>
            <Users />
          </div>
        </div>
      </div>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    display: 'flex',
    flexDirection: 'row',
    height: '100vh',
    background: '#e8e9ed',
  },

  sidebar: {
    width: 80,
    background: '#38386e',
    boxShadow: '0px 0px 5px 0px rgba(0,0,0,0.75)',
  },

  logo: {
    width: 'calc(100% - 20px)',
    margin: 10
  },

  menu: {
    listStyle: 'none',
    margin: '10px 0 0 0',
    padding: 0,
    fontFamily: 'Nunito, sans-serif',
    ':nth-child(1n) > li > a': {
      display: 'flex',
      flexDirection: 'column',
      textAlign: 'center',
      color: '#fff',
      textDecoration: 'none',
      fontWeight: 100,
      fontSize: 12,
      padding: '15px 0',
      borderBottom: '1px solid #282858',
      textTransform: 'uppercase',
      ':nth-child(1n) > i': {
        fontSize: 30,
        marginBottom: 5
      },
      ':hover': {
        background: '#57386e'
      }
    },
    ':nth-child(1n) > li:last-child > a': {
      borderBottom: 0
    },
    ':nth-child(1n) > li > a.active': {
      background: '#57386e'
    }
  },

  main: {
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
  },

  header: {
    background: '#fff',
    padding: 15,
    boxShadow: '0px 0px 5px 0px rgba(0,0,0,0.75)',
    textAlign: 'right',
    position: 'relative'
  },

  avatar: {
    marginRight: 10
  },

  content: {
    flex: 1,
    overflowY: 'auto'
  },
})

export default connect(state => ({ user: state.user, router: state.router }))(Admin)