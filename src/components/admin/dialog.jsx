import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'
import { toast } from 'react-toastify'

import RaisedButton from 'material-ui/RaisedButton'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import IconButton from 'material-ui/IconButton'

import * as usersActions from '../../actions/users'

const customContentStyle = {
  width: 300,
};
// eslint-disable-next-line
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const defForm = {
  FirstName: '',
  LastName: '',
  Email: '',
  PhoneNumber: '',
  Password: '',
  PasswordConfirm: '',
  IsActive: true,
  IsEnable: true,
  RoleIds: []
}

class UserDialog extends Component {

  state = {
    open: false,
    form: { ...defForm } 
  }

  handleOpen() {
    this.setState({ open: true, form: this.props.user ? { 
      ...this.props.user,
      Password: null,
      PasswordConfirm: null
    } : { ...defForm } });
  }

  handleClose() {
    this.setState({ open: false });
  }

  onChangeForm(field, value){
    this.setState({ 
      form: {
        ...this.state.form,
        [field]: value
      }
    })
  }

  async onSubmit(){
    const {form} = this.state;

    try{
      if (form.FirstName === '') throw new Error('FirstName is required field')
      if (form.LastName === '') throw new Error('LastName is required field')
      if (!emailRegex.test(form.Email)) throw new Error('Email is invalid')

      if(this.props.user){
        if (form.Password !== null && (form.Password === '' || form.Password.length < 6)) throw new Error('Password is required field and length must be least 6 symbols')
        if (form.Password !== null && (form.Password !== form.PasswordConfirm)) throw new Error('Password confirm is invalid')

         await usersActions.edit(form);

        toast.success('User successfully updated')
      } else {
        if (form.Password === '' || form.Password.length < 6) throw new Error('Password is required field and length must be least 6 symbols')
        if (form.Password !== form.PasswordConfirm) throw new Error('Password confirm is invalid')

        await usersActions.add(form);

        toast.success('User successfully created')
      }
 
      await usersActions.get()
      this.setState({ open: false })
    } catch(e) {
      console.log(e)
      toast.error(e.message)
    }
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        onClick={() => this.handleClose()}
      />,
      <FlatButton
        label="Submit"
        primary={true}
        onClick={() => this.onSubmit()}
      />
    ]

    return (
      <div className={css(styles.wrapper)}>
        { this.props.user ? (
          <IconButton onClick={() => this.handleOpen()}>
            <i className={css(styles.actions) + " material-icons"}>mode_edit</i>
          </IconButton>
        ) : (
          <RaisedButton
            label="Create user"
            primary
            onClick={() => this.handleOpen()}
            icon={<i className="material-icons">add</i>}
            className={css(styles.addBtn)} />
        ) }
        
        <Dialog
          title="User form"
          actions={actions}
          modal={true}
          contentStyle={customContentStyle}
          open={this.state.open}
          autoScrollBodyContent={true}
        >
          <TextField
            hintText="Enter your First Name"
            floatingLabelText="First Name"
            floatingLabelFixed={true}
            fullWidth
            value={this.state.form.FirstName}
            onChange={(e, value) => this.onChangeForm('FirstName', value)}
          />
          <TextField
            hintText="Enter your Last Name"
            floatingLabelText="Last Name"
            floatingLabelFixed={true}
            fullWidth
            value={this.state.form.LastName}
            onChange={(e, value) => this.onChangeForm('LastName', value)}
          />
          <TextField
            hintText="Enter your email"
            floatingLabelText="Email"
            floatingLabelFixed={true}
            fullWidth
            value={this.state.form.Email}
            onChange={(e, value) => this.onChangeForm('Email', value)}
            disabled={this.props.user !== undefined}
          />
          <TextField
            hintText="Enter your phone number"
            floatingLabelText="Phone number"
            floatingLabelFixed={true}
            fullWidth
            value={this.state.form.PhoneNumber ? this.state.form.PhoneNumber : ''}
            onChange={(e, value) => this.onChangeForm('PhoneNumber', value)}
          />
          <TextField
            hintText="Enter your password"
            floatingLabelText="Password"
            floatingLabelFixed={true}
            fullWidth
            value={this.state.form.Password ? this.state.form.Password : ''}
            onChange={(e, value) => this.onChangeForm('Password', value)}
            type="password"
          />
          <TextField
            hintText="Enter your password confirm"
            floatingLabelText="Password confirm"
            floatingLabelFixed={true}
            fullWidth
            value={this.state.form.PasswordConfirm ? this.state.form.PasswordConfirm : ''}
            onChange={(e, value) => this.onChangeForm('PasswordConfirm', value)}
            type="password"
          />
        </Dialog>
      </div>
    )
  }

}

const styles = StyleSheet.create({
  wrapper: {
    display: 'inline-block'
  },

  addBtn: {
    ':nth-child(1n) i': {
      color: '#fff',
      top: -1,
      position: 'relative'
    }
  },

  actions: {
    color: '#588b8b'
  }
})

export default UserDialog