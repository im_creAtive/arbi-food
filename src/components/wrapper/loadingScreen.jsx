import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import { css, StyleSheet } from 'aphrodite'

import CircularProgress from 'material-ui/CircularProgress';

class LoadingScreen extends PureComponent {
  render() {
    return (
      <div className={css(styles.wrapper, this.props.env.count > 0 && styles.active)}>
        {this.props.env.count > 1 ? (
          <div className={css(styles.loader)}>
            <CircularProgress
              size={100}
              thickness={7}
              mode="determinate"
              max={this.props.env.total}
              value={this.props.env.total - this.props.env.count} />
            <br />
            <br />
            Loading {this.props.env.total - this.props.env.count}/{this.props.env.total}, please wait...
          </div>
        ) : (
            <div className={css(styles.loader)}>
              <div>
                <CircularProgress size={100} thickness={7} />
                <br />
                <br />
                Loading, please wait...
            </div>
            </div>
          )}
      </div>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    zIndex: -1,
    opacity: 0,
    transition: '0.5s',
    display: 'flex',
    justifyContent: 'center',
    ':after': {
      content: "''",
      position: 'absolute',
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
      background: '#588b8b',
      opacity: 0,
      transition: '0.5s',
      zIndex: -1
    }
  },

  active: {
    zIndex: 100000,
    opacity: 1,
    transition: '0.5s',
    ':after': {
      opacity: 0.5,
      transition: '0.5s',
    }
  },

  loader: {
    textAlign: 'center',
    alignSelf: 'center',
    color: '#57386e',
    fontSize: 22,
    fontWeight: 'bold'
  }
})

export default connect(state => ({ env: state.env }))(LoadingScreen);