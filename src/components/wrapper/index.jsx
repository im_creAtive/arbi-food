import React, { Component } from 'react'
import { Switch, Route } from 'react-router'
import { toast } from 'react-toastify'
import { withRouter } from 'react-router'

import Login from '../login'
import Admin from '../admin'
import User from '../user'
import Landing from '../landing'

import Api from '../../actions/api'
import * as usersActions from '../../actions/users'

class Wrapper extends Component{

  state = {
    loaded: false
  }

  async componentDidMount(){
    // auth check
    try{
      if (Api.getTokenFromLocalStorage()){
        let user = await usersActions.currentUser()

        if(!user.Id) throw new Error('Token is invalid. Please try sign in again.')
      }

      this.setState({ loaded: true })
    } catch(e) {
      console.log(e)
      toast.error(e.message)
      Api.deleteTokenFromLocalStorage()
      this.props.history.push('/login')
      this.setState({ loaded: true })
    }
  }

  render(){
    const {loaded} = this.state

    console.log(this.props)

    return loaded ? (
      <Switch>
        { /* <Redirect exact from="/" to="/login" /> */ }
        <Route exact path="/" component={Landing} />
        <Route path="/login" component={Login} />
        <Route path="/user" component={User} />
        <Route path="/admin" component={Admin} />
      </Switch>
    ) : null
  }
}

export default withRouter(Wrapper)