import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'
import { connect } from 'react-redux'
import { toast } from 'react-toastify'

// import TextField from 'material-ui/TextField'
// import RaisedButton from 'material-ui/RaisedButton'
// import Checkbox from 'material-ui/Checkbox'

// import icon from '../../sources/img/icon.png'
// import facebookLogo from '../../sources/img/facebook-logo.png'
// import linkedinLogo from '../../sources/img/linkedin-logo.png'

import * as loginActions from '../../actions/login'
import * as usersActions from '../../actions/users'
import * as selectedUserActions from '../../actions/selectedUser'

import Header from '../landing/components/header'
import Footer from '../landing/components/footer'

const localStorageLogin = localStorage.getItem('remember')
const defState = {
  login: localStorageLogin ? localStorageLogin : '',
  password: "",
  rememberMe: localStorageLogin !== null
}

class Login extends Component{

  state = { ...defState }

  constructor(props){
    super(props);

    if(this.props.user.Id){
      if (this.props.user.RoleIds.indexOf('Admin') > -1 && !selectedUserActions.getSelectedUserId()) {
        this.props.history.push('/admin')
      } else {
        this.props.history.push('/user')
      }
    }
  }

  async onSubmit(e){
    e.preventDefault();

    try{
      await loginActions.token(this.state.login, this.state.password)
      let user = await usersActions.currentUser();

      if(this.state.rememberMe){
        localStorage.setItem('remember', this.state.login)
      } else {
        localStorage.removeItem('remember')
      }

      if (user.RoleIds.indexOf('Admin') > -1){
        this.props.history.replace('/admin')
      } else {
        this.props.history.replace('/user')
      }
    } catch(e) {
      console.log(e)
      toast.error(e.message)
    }
  }

  render(){
    return (
      <div className={css(styles.wrapper)}>
        { /* <div className={css(styles.header)}>
          <div>
            <img className={css(styles.icon)} src={icon} alt="logo" />
            <a className={css(styles.link)} href='https://arbi.io/'>Go to Arbi.io</a>
          </div>
        </div> */ }
        <Header />
        <div className={css(styles.login)}>
          <div className={css(styles.loginBlock)}>
            { /* <h2>Already have an account?</h2>
            <p>Login to your account</p> */ }
            <form className={css(styles.loginForm)} onSubmit={e => this.onSubmit(e)}>
              <h3>Welcome</h3>
              { /* <TextField
                hintText="Enter your email"
                floatingLabelText="Login"
                floatingLabelFixed={true}
                fullWidth
                value={this.state.login}
                onChange={(e, text) => this.setState({ login: text })}
              />
              <TextField
                hintText="Enter your password"
                floatingLabelText="Password"
                floatingLabelFixed={true}
                fullWidth
                type="password"
                value={this.state.password}
                onChange={(e, text) => this.setState({ password: text })}
              />
              <Checkbox
                label="Remember me"
                className={css(styles.rememberMe)}
                checked={this.state.rememberMe}
                onCheck={() => this.setState({ rememberMe: !this.state.rememberMe })}
              />
              <RaisedButton 
                icon={<i className="material-icons">lock_open</i>} 
                label="Sign In" 
                primary={true} 
                fullWidth
                className={css(styles.loginBtn)}
                type="submit" /> */ }
              
              <div className={css(styles.input)}>
                <input 
                  placeholder="Login"
                  value={this.state.login}
                  onChange={(e) => this.setState({ login: e.target.value })} />
                <i className="material-icons">email</i>
              </div>
              <div className={css(styles.input)}>
                <input 
                  placeholder="Password"
                  type="password"
                  value={this.state.password}
                  onChange={(e) => this.setState({ password: e.target.value })} />
                <i className="material-icons">vpn_key</i>
              </div>
              <label>
                <input 
                  type="checkbox"
                  checked={this.state.rememberMe}
                  onClick={() => this.setState({ rememberMe: !this.state.rememberMe })} /> Remember me
              </label>

              <button className={css(styles.button)} type="submit">Login</button>
            </form>
          </div>
        </div>
        <Footer />
        { /* <div className={css(styles.footer)}>
          <div>
            <div>©2018 Arbi All Right Reserved.</div>
            <ul className={css(styles.socials)}>
              <li><a href="https://www.facebook.com/arbiapp/" target="_blank" rel="noopener noreferrer"><img src={facebookLogo} alt="facebook" /></a></li>
              <li><a href="https://www.linkedin.com/company/17998258/" target="_blank" rel="noopener noreferrer"><img src={linkedinLogo} alt="inkedin" /></a></li>
            </ul>
          </div>
        </div> */ }
      </div>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    // display: 'flex',
    // flexDirection: 'column',
    background: '#fff',
  },

  header: {
    height: 50,
    background: 'linear-gradient(to right, #36658a 0%,#36658a 50%,#526ba1 51%,#526ba1 100%);',
    boxShadow: '0px 0px 5px 0px rgba(0,0,0,0.75)',
    fontFamily: 'Nunito, sans-serif',
    ':nth-child(1n) > div': {
      width: '100%',
      maxWidth: 1000,
      background: '#526ba1',
      margin: '0 auto'
    }
  },

  icon: {
    height: 46
  },

  link: {
    color: '#fff',
    float: 'right',
    marginTop: 12,
    textDecoration: 'none',
    ':hover': {
      textDecoration: 'underline',      
    }
  },  

  login: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    padding: '175px 0 100px 0'
  },

  loginBlock: {
    alignSelf: 'center',
    textAlign: 'center',
    fontFamily: 'Nunito, sans-serif'
  },

  loginForm: {
    width: 300,
    padding: 15,
    borderRadius: 10,
    boxShadow: '0px 0px 21px 0px rgba(158,158,158,1)',
    background: '#fff',
    textAlign: 'left',
    ':nth-child(1n) > h3': {
      textAlign: 'center',
      margin: '5px 0 30px 0'
    }
  },

  input: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 15,
    ':nth-child(1n) > input': {
      flex: 1,
      background: '#e9e7e8',
      border: '1px solid #c8c5c6',
      color: '#484848',
      padding: 7,
      borderRight: 0,
      borderTopLeftRadius: 3,
      borderBottomLeftRadius: 3,
      ':focus': {
        outline: 0
      }
    },
    ':nth-child(1n) > i': {
      background: '#f05272',
      color: '#fff',
      padding: '6px 8px',
      fontSize: 18,
      borderTopRightRadius: 3,
      borderBottomRightRadius: 3
    }
  },

  button: {
    display: 'block',
    marginTop: 15,
    background: '#f05272',
    color: '#fff',
    width: '100%',
    padding: 8,
    fontWeight: 'bold',
    border: 0,
    borderRadius: 3,
    cursor: 'pointer',
    ':hover': {
      background: '#d84a66'
    },
    ':focus': {
      outline: 0
    }
  }

  /*rememberMe: {
    margin: '10px 0 15px 0'
  },  

  loginBtn: {
    ':nth-child(1n) i': {
      color: '#fff',
    }
  },

  footer: {
    background: '#38386e',
    padding: '20px 10px',
    color: '#fff',
    fontFamily: 'Nunito, sans-serif',
    ':nth-child(1n) > div': {
      width: '100%',
      maxWidth: 1000,
      margin: '0 auto',
      display: 'flex',
      justifyContent: 'space-between'
    }
  },

  socials: {
    listStyle: 'none',
    padding: 0,
    margin: 0,
    float: 'right',
    ':nth-child(1n) > li': {
      float: 'left',
      marginLeft: 10
    }
  }*/
})

export default connect(state => ({ user: state.user }))(Login)