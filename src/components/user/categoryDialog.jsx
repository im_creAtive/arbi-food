import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'
import { toast } from 'react-toastify'
import { connect } from 'react-redux'

import RaisedButton from 'material-ui/RaisedButton'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import IconButton from 'material-ui/IconButton'
import { ListItem } from 'material-ui/List'
import Avatar from 'material-ui/Avatar'
import Chip from 'material-ui/Chip'

import * as categoryActions from '../../actions/categories'
import * as fileActions from '../../actions/files'
import * as dishActions from '../../actions/dish'

const customContentStyle = {
  width: 600,
}
const defForm = {
  Name: "",
  Image: null,
  Information: ""
}

class CategoryDialog extends Component {

  state = {
    open: false,
    form: { ...defForm }
  }

  handleOpen(e) {
    e.preventDefault();

    this.setState({ open: true, form: this.props.category ? { ...this.props.category } : { ...defForm } });
  }

  handleClose() {
    delete this.file
    this.setState({ open: false });
  }

  onChangeForm(field, value) {
    this.setState({
      form: {
        ...this.state.form,
        [field]: value
      }
    })
  }

  async onSubmit() {
    const { form } = this.state;

    try {
      if (form.Name === '') throw new Error('Name is required field')

      if(form.Id){
        await categoryActions.edit(form);
        await categoryActions.get(this.props.restaurant.Id)
        toast.success('Category successfully updated')
      } else {
        let category = await categoryActions.add(this.props.restaurant.Id, form);
        if (this.file){
          await categoryActions.upload(this.file, category.Id)
          delete this.file
        }
        await categoryActions.get(this.props.restaurant.Id)
        
        toast.success('Category successfully created')
      }

      this.setState({ open: false })
    } catch (e) {
      console.log(e)
      toast.error(e.message)
    }
  }

  async onChangeImage(e) {
    if (e.target.files.length < 1) return;

    const file = e.target.files[0];
    e.target.value = null;

    if(!this.props.category){
      this.file = file;
      this.forceUpdate();
      return;
    }

    try {
      let response = await categoryActions.upload(file, this.props.category.Id)
      await categoryActions.get(this.props.restaurant.Id)

      console.log(response)
      toast.success('Image successfully uploaded')
    } catch (e) {
      console.log(e)
      toast.error(e.message)
    }
  }

  async onDelete(){
    if (!window.confirm('Do you confirm the deletion?')) return;

    try{
      await categoryActions.del(this.props.category.Id);
      this.setState({ open: false }, async () => {
        await categoryActions.get(this.props.restaurant.Id)
        await dishActions.get(this.props.restaurant.Id)
        toast.success('Category successfully deleted')
      })
    } catch(e) {
      console.log(e)
      toast.error(e.message)
    }
  }

  render() {
    let containDishes = 0
    if (this.props.category) {
      for (let i in this.props.dishes.list) {
        if (this.props.dishes.list[i].CategoriesOfDishId === this.props.category.Id) containDishes++
      }
    }

    const actions = [
      this.props.category && <FlatButton
        label="Delete"
        onClick={() => this.onDelete()}
        style={{ float: 'left' }}
        icon={<i className={css(styles.iconRemove) + " material-icons"}>delete</i>}
      />,
      <FlatButton
        label="Cancel"
        onClick={() => this.handleClose()}
      />,
      <FlatButton
        label="Save"
        primary={true}
        onClick={() => this.onSubmit()}
      />
    ]

    const dialog = (
      <Dialog
        title="Category"
        actions={actions}
        modal={true}
        contentStyle={customContentStyle}
        open={this.state.open}
        autoScrollBodyContent={true}
      >
        <div className={css(styles.sections)}>
          <div>
            <TextField
              hintText="Enter category name"
              floatingLabelText="Name"
              floatingLabelFixed={true}
              fullWidth
              value={this.state.form.Name}
              onChange={(e, value) => this.onChangeForm('Name', value)}
            />
            <TextField
              hintText="Enter category info"
              floatingLabelText="Information"
              floatingLabelFixed={true}
              fullWidth
              value={this.state.form.Information}
              onChange={(e, value) => this.onChangeForm('Information', value)}
              multiLine={true}
              rows={3}
              rowsMax={6}
            />
          </div>
          <div>
            <div className={css(styles.noPhoto)}>
              {this.props.category && this.props.category.Image !== '' && this.props.category.Image !== null ? (
                <img src={fileActions.categoryPhoto(this.props.category.Image)} alt="category logo"  className={css(styles.categoryImage)} />
              ) : (
                <div>Upload image of category</div>
              ) }
              {this.file && (
                <Chip className={css(styles.file)}>
                  <Avatar icon={<i className="material-icons">insert_drive_file</i>} />
                  {this.file.name}
                </Chip>
              )}
              <FlatButton
                label="Upload"
                secondary
                icon={<i className="material-icons">file_upload</i>}
                className={css(styles.uploadBtn)}
              >
                <input type="file" className={css(styles.uploadInput)} onChange={e => this.onChangeImage(e)} ref={node => this.fileInput = node} />
              </FlatButton>
            </div>
          </div>
        </div>
      </Dialog>
    )

    return this.props.category ? (
      <ListItem
        leftAvatar={<Avatar icon={<i className="material-icons">folder</i>} />}
        rightIconButton={<IconButton onClick={e => this.handleOpen(e)}><i className={css(styles.actions) + " material-icons"}>mode_edit</i>{dialog}</IconButton>}
        primaryText={this.props.category.Name}
        secondaryText={"Contains " + containDishes + " dish(es)"}
      />
    ) : (
      <RaisedButton
        label="Create category"
        fullWidth
        secondary
        className={css(styles.addBtn)}
        icon={<i className="material-icons">add</i>}
        onClick={e => this.handleOpen(e)}>
        {dialog}
      </RaisedButton>
    )
  }

}

const styles = StyleSheet.create({
  wrapper: {
    display: 'inline-block'
  },

  wrapperBlock: {
    display: 'block'
  },

  sections: {
    display: 'flex',
    ':nth-child(1n) > div': {
      flex: 1,
    },
    ':nth-child(1n) > div:last-child': {
      borderLeft: '1px solid #e0e0e0',
      marginLeft: 15,
      paddingLeft: 15,
      display: 'flex',
      justifyContent: 'center'
    }
  },

  noPhoto: {
    alignSelf: 'center'
  },

  uploadBtn: {
    margin: '10px auto 0 auto !important',
    display: 'block !important',
    position: 'relative !important',
    verticalAlign: 'middle !important',
    cursor: 'pointer !important',
  },

  uploadInput: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    width: '100%',
    opacity: 0,
    zIndex: 2,
    cursor: 'pointer !important'
  },  

  addBtn: {
    ':nth-child(1n) i': {
      color: '#fff',
      top: -1,
      position: 'relative'
    }
  },

  actions: {
    color: '#588b8b'
  },

  file: {
    margin: '15px auto 0 auto !important'
  },

  categoryImage: {
    maxWidth: '100%',
    background: '#f1f1f1',
    display: 'block',
    margin: '0 auto',
  },

  iconRemove: {
    fontSize: '20px !important',
    position: 'relative',
    top: -1
  }
})

export default connect(state => ({ 
  user: state.user, 
  restaurant: state.restaurant,
  dishes: state.dishes
}))(CategoryDialog)