import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite'
import { connect } from 'react-redux'

import Paper from 'material-ui/Paper'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
} from 'material-ui/Table'
import { List } from 'material-ui/List'

import CategoryDialog from './categoryDialog'
import DishDialog from './dishDialog'

class Dishes extends Component {

  componentDidMount() {

  }

  render() {
    const dishes = this.props.dishes.list.map((dish, index) => {
      return <DishDialog key={index} dish={dish} />
    })

    const categories = this.props.categories.list.map((category, index) => {
      return <CategoryDialog key={index} category={category} />
    })

    return (
      <div className={css(styles.wrapper)}>

        <div>
          <CategoryDialog />

          { categories.length < 1 ? (
            <div className={css(styles.emptyCategories)}>
              <span><i className="material-icons">info</i>Categories list is empty</span>
            </div>
          ): <List>{ categories }</List> }
        </div>
        <div>
          <Paper zDepth={1} className={css(styles.title)}>
            <span>Dishes</span>
            <DishDialog />
          </Paper>
          
          { dishes.length < 1 ? (
            <div className={css(styles.emptyCategories)}>
              <span><i className="material-icons">info</i>Dishes list is empty</span>
            </div>
          ) : (
            <Paper zDepth={ 1 } style={{ marginTop: 15 }}>
              <Table selectable={false}>
              <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                <TableRow>
                  <TableHeaderColumn>Photo</TableHeaderColumn>
                  <TableHeaderColumn>Name</TableHeaderColumn>
                  <TableHeaderColumn>Category</TableHeaderColumn>
                  <TableHeaderColumn>Price</TableHeaderColumn>
                  <TableHeaderColumn style={{ width: 96 }}>Actions</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={false}>
                {dishes}
              </TableBody>
            </Table>
          </Paper>
          ) }
        </div>

      </div>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    display: 'flex',
    flexDirection: 'row',
    height: '100%',
    ':nth-child(1n) > div:first-child': {
      width: 300,
      padding: '15px 0 15px 15px',
      display: 'flex',
      flexDirection: 'column'
    },
    ':nth-child(1n) > div:last-child': {
      flex: 1,
      overflowY: 'auto',
      padding: 15,
      display: 'flex',
      flexDirection: 'column'
    },
  },
  
  addBtn: {
    ':nth-child(1n) i': {
      color: '#fff',
      top: -1,
      position: 'relative'
    }
  },

  actions: {
    color: '#588b8b'
  },

  title: {
    padding: 10,
    display: 'flex',
    justifyContent: 'space-between',
    minHeight: 55,
    ':nth-child(1n) > span': {
      fontSize: 20,
      fontWeight: 'bold',
      padding: '6px 0',
      color: '#5f5f5f'
    }
  },

  table: {
    marginTop: 15
  },

  emptyCategories: {
    display: 'flex',
    justifyContent: 'center',
    textAlign: 'center',
    flex: 1,
    marginTop: 15,
    background: '#dfe0e4',
    ':nth-child(1n) > span': {
      alignSelf: 'center',
      display: 'flex',
      flexDirection: 'column',
      color: '#a2a2a2',
      ':nth-child(1n) i': {
        fontSize: 40,
        marginBottom: 10
      }
    }
  }
})

export default connect(state => ({ 
  user: state.user, 
  restaurant: state.restaurant,
  categories: state.categories,
  dishes: state.dishes
 }))(Dishes)