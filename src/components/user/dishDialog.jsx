import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'
import { toast } from 'react-toastify'
import { connect } from 'react-redux'

import RaisedButton from 'material-ui/RaisedButton'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'
import IconButton from 'material-ui/IconButton'
import Avatar from 'material-ui/Avatar'
import Chip from 'material-ui/Chip'
import SelectField from 'material-ui/SelectField'
import MenuItem from 'material-ui/MenuItem'
import {
  TableRow,
  TableRowColumn,
} from 'material-ui/Table'

import * as dishActions from '../../actions/dish'
import * as fileActions from '../../actions/files'

const customContentStyle = {
  width: 1000,
}
const defForm = {
  Name: "",
  CategoriesOfDishId: null,
  Information: "",
  Price: ""
}

class DishDialog extends Component {

  state = {
    open: false,
    form: { ...defForm }
  }
  files = []

  handleOpen(e) {
    e.preventDefault();

    this.setState({ open: true, form: this.props.dish ? { ...this.props.dish } : { ...defForm } });
  }

  handleClose() {
    delete this.file
    this.setState({ open: false });
  }

  onChangeForm(field, value) {
    this.setState({
      form: {
        ...this.state.form,
        [field]: value
      }
    })
  }

  async onSubmit() {
    const { form } = this.state;

    try {
      if (form.CategoriesOfDishId === null) throw new Error('Category is required field')
      if (form.Name === '') throw new Error('Name is required field')
      if (form.Information === '') throw new Error('Information is required field')
      if (form.Price === '') throw new Error('Price is required field')
      if (isNaN(form.Price)) throw new Error('Price is invalid')

      if (form.Id) {
        await dishActions.edit(form);
        await dishActions.get(this.props.restaurant.Id)
        toast.success('Category successfully updated')
      } else {
        let dish = await dishActions.add(form);
        if (this.photo) {
          await dishActions.uploadPhoto(this.photo, dish.Id)
          delete this.photo
        }
        if(this.files.length > 0){
          await dishActions.uploadFiles(this.files, dish.Id)
          this.files = []
        }
        await dishActions.get(this.props.restaurant.Id)

        toast.success('Dish successfully created')
      }

      this.setState({ open: false })
    } catch (e) {
      console.log(e)
      toast.error(e.message)
    }
  }

  async onChangeImage(e) {
    if (e.target.files.length < 1) return;

    const file = e.target.files[0];
    e.target.value = null;

    if (!this.props.dish) {
      this.photo = file;
      this.forceUpdate();
      return;
    }

    try {
      let response = await dishActions.uploadPhoto(file, this.props.dish.Id)
      await dishActions.get(this.props.restaurant.Id)

      console.log(response)
      toast.success('Photo successfully uploaded')
    } catch (e) {
      console.log(e)
      toast.error(e.message)
    }
  }

  async onChangeFile(e){
    if (e.target.files.length < 1) return;

    const files = Array.from(e.target.files);
    e.target.value = null;

    if (!this.props.dish) {
      this.files = this.files.concat(files);
      console.log(this.files);
      this.forceUpdate();
      return;
    }

    try{
      await dishActions.uploadFiles(files, this.props.dish.Id)
      await dishActions.get(this.props.restaurant.Id)

      toast.success('Files successfully uploaded')
    } catch(e) {
      console.log(e)
      toast.error(e.message)
    }
  }

  async onDelete() {
    if (!window.confirm('Do you confirm the deletion?')) return;

    try {
      await dishActions.del(this.props.dish.Id);
      this.setState({ open: false }, async () => {
        await dishActions.get(this.props.restaurant.Id)
        toast.success('Dish successfully deleted')
      })
    } catch (e) {
      console.log(e)
      toast.error(e.message)
    }
  }

  onRemoveFile(index){
    this.files.splice(index, 1)
    this.forceUpdate()
  }

  onDownloadFile(params){
    window.open(fileActions.dishPhoto(params));
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        onClick={() => this.handleClose()}
      />,
      <FlatButton
        label="Save"
        primary={true}
        onClick={() => this.onSubmit()}
      />
    ]

    const categories = this.props.categories.list.map((category, index) => {
      return (
        <MenuItem key={index} value={category.Id} primaryText={category.Name} />
      )
    })

    const category = this.props.dish ? this.props.categories.list.find(category => category.Id === this.props.dish.CategoriesOfDishId) : null

    const dialog = (
      <Dialog
        title="Dish"
        actions={actions}
        modal={true}
        contentStyle={customContentStyle}
        open={this.state.open}
        autoScrollBodyContent={true}
      >
        <div className={css(styles.sections)}>
          <div>
            <SelectField
              floatingLabelText="Category"
              value={this.state.form.CategoriesOfDishId ? this.state.form.CategoriesOfDishId : 'list'}
              onChange={(e, i, value) => this.onChangeForm('CategoriesOfDishId', value)}
            >
              <MenuItem value="list" primaryText="Select category" disabled />
              {categories}
            </SelectField>
            <TextField
              hintText="Enter dish name"
              floatingLabelText="Name"
              floatingLabelFixed={true}
              fullWidth
              value={this.state.form.Name}
              onChange={(e, value) => this.onChangeForm('Name', value)}
            />
            <TextField
              hintText="Enter dish info"
              floatingLabelText="Information"
              floatingLabelFixed={true}
              fullWidth
              value={this.state.form.Information}
              onChange={(e, value) => this.onChangeForm('Information', value)}
              multiLine={true}
              rows={3}
              rowsMax={6}
            />
            <TextField
              hintText="Enter dish price"
              floatingLabelText="Price"
              floatingLabelFixed={true}
              fullWidth
              value={this.state.form.Price}
              onChange={(e, value) => this.onChangeForm('Price', value)}
            />
          </div>
          <div>
            <div className={css(styles.noPhoto)}>
              {this.props.dish && this.props.dish.Photo !== '' && this.props.dish.Photo !== null ? (
                <img src={fileActions.dishPhoto(this.props.dish.Photo)} alt="dish logo" className={css(styles.categoryImage)} />
              ) : (
                  <div>Upload photo of the dish (Optional)</div>
                )}
              {this.photo && (
                <Chip className={css(styles.file)}>
                  <Avatar icon={<i className="material-icons">insert_drive_file</i>} />
                  {this.photo.name}
                </Chip>
              )}
              <FlatButton
                label="Upload"
                secondary
                icon={<i className="material-icons">file_upload</i>}
                className={css(styles.uploadBtn)}
              >
                <input type="file" className={css(styles.uploadInput)} onChange={e => this.onChangeImage(e)} ref={node => this.fileInput = node} />
              </FlatButton>
            </div>
          </div>
          <div>
            <div className={css(styles.noPhoto)}>
              <div style={{ textAlign: 'center' }}>
                Upload files to the dish
                  <br />
                { /* <strong><code>(.obj .mtl .zip .png .jpg)</code></strong> */ }
              </div>
              {this.props.dish && this.props.dish.Files.length > 0 ? this.props.dish.Files.map((file, index) => {
                let parts = file.Url.split('=')
                let fileName = parts[parts.length - 1];

                return (
                  <Chip className={css(styles.file)} key={index} onClick={() => this.onDownloadFile(file.Url)}>
                    <Avatar icon={<i className="material-icons">insert_drive_file</i>} />
                    {fileName}
                  </Chip>
                )
              }) : null}
              { this.files.length > 0 && this.files.map((file, index) => {
                return (
                  <Chip className={css(styles.file)} key={index} onRequestDelete={() => this.onRemoveFile(index)}>
                    <Avatar icon={<i className="material-icons">insert_drive_file</i>} />
                    {file.name}
                  </Chip>
                )
              }) }
              <FlatButton
                label="Upload"
                secondary
                icon={<i className="material-icons">file_upload</i>}
                className={css(styles.uploadBtn)}
              >
                <input type="file" multiple className={css(styles.uploadInput)} onChange={e => this.onChangeFile(e)} ref={node => this.fileInputFile = node} />
              </FlatButton>
            </div>
          </div>
        </div>
      </Dialog>
    )

    return this.props.dish ? (
      <TableRow>
        <TableRowColumn>
          { this.props.dish.Photo !== '' && this.props.dish.Photo !== null ? (
            <Avatar src={fileActions.dishPhoto(this.props.dish.Photo)} />
          ) : (
            <Avatar icon={<i className="material-icons">photo_camera</i>} />
          )}
        </TableRowColumn>
        <TableRowColumn>{this.props.dish.Name}</TableRowColumn>
        <TableRowColumn>{category ? category.Name : '-'}</TableRowColumn>
        <TableRowColumn>{this.props.dish.Price}</TableRowColumn>
        <TableRowColumn style={{ width: 96 }}>
          <IconButton onClick={e => this.handleOpen(e)}>
            <i className={css(styles.actions) + " material-icons"}>mode_edit</i>
          </IconButton>
          <IconButton onClick={() => this.onDelete(this.props.dish.Id)}>
            <i className={css(styles.actions) + " material-icons"}>delete</i>
          </IconButton>
          {dialog}
        </TableRowColumn>
      </TableRow>
    ) : (
      <RaisedButton
        label="Create dish"
        primary
        icon={<i className="material-icons">add</i>}
        className={css(styles.addBtn)}
        onClick={e => this.handleOpen(e)}>
        { dialog }
      </RaisedButton>
      )
  }

}

const styles = StyleSheet.create({
  wrapper: {
    display: 'inline-block'
  },

  wrapperBlock: {
    display: 'block'
  },

  sections: {
    display: 'flex',
    ':nth-child(1n) > div': {
      flex: 1,
    },
    ':nth-child(1n) > div:nth-child(2)': {
      borderLeft: '1px solid #e0e0e0',
      marginLeft: 15,
      paddingLeft: 15,
      display: 'flex',
      justifyContent: 'center',
      flex: 1
    },
    ':nth-child(1n) > div:nth-child(3)': {
      borderLeft: '1px solid #e0e0e0',
      marginLeft: 15,
      paddingLeft: 15,
      display: 'flex',
      justifyContent: 'center',
      flex: 1
    }
  },

  noPhoto: {
    alignSelf: 'center',
    textAlign: 'center'
  },

  uploadBtn: {
    margin: '10px auto 0 auto !important',
    display: 'block !important',
    position: 'relative !important',
    verticalAlign: 'middle !important',
    cursor: 'pointer !important',
  },

  uploadInput: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    width: '100%',
    opacity: 0,
    zIndex: 2,
    cursor: 'pointer !important'
  },

  addBtn: {
    ':nth-child(1n) i': {
      color: '#fff',
      top: -1,
      position: 'relative'
    }
  },

  actions: {
    color: '#588b8b'
  },

  file: {
    margin: '15px auto 0 auto !important'
  },

  categoryImage: {
    maxWidth: '100%',
    background: '#f1f1f1',
    display: 'block',
    margin: '0 auto',
  }
})

export default connect(state => ({ 
  user: state.user, 
  restaurant: state.restaurant,
  categories: state.categories,
  dishes: state.dishes
}))(DishDialog)