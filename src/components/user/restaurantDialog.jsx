import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'
import { toast } from 'react-toastify'
import { connect } from 'react-redux'

import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import TextField from 'material-ui/TextField'

import * as restaurantActions from '../../actions/restaurant'
import * as fileActions from '../../actions/files'

const customContentStyle = {
  width: 600,
}

class RestaurantDialog extends Component {

  state = {
    open: false,
    form: {}
  }

  handleOpen(e) {
    e.preventDefault();

    this.setState({ open: true, form: { ...this.props.restaurant } });
  }

  handleClose() {
    this.setState({ open: false });
  }

  onChangeForm(field, value) {
    this.setState({
      form: {
        ...this.state.form,
        [field]: value
      }
    })
  }

  async onSubmit() {
    const { form } = this.state;

    try {
      if (form.NameRestaurant === '') throw new Error('Restaurant name is required field')
      //if (form.AdressRestaurant === '') throw new Error('Restaurant address is required field')


      await restaurantActions.edit(form);
      await restaurantActions.current()

      toast.success('Your restaurant successfully updated')
      this.handleClose()
    } catch (e) {
      console.log(e)
      toast.error(e.message)
    }
  }

  async onChangeImage(e){
    if (e.target.files.length < 1) return;

    const file = e.target.files[0];
    e.target.value = null;

    try{
      let response = await restaurantActions.upload(file, this.props.restaurant.Id)
      await restaurantActions.current();

      this.setState({ 
        form: {
          ...this.state.form,
          Photo: response.Photo
        } 
      });
      toast.success('Image successfully uploaded')
    } catch(e) {
      console.log(e)
      toast.error(e.message)
    }
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        onClick={() => this.handleClose()}
      />,
      <FlatButton
        label="Save"
        primary={true}
        onClick={() => this.onSubmit()}
      />
    ]

    return (
      <li>
        <a 
          href="" 
          onClick={e => this.handleOpen(e) }
          className={this.state.open ? 'active' : ''}>
          <i className="material-icons">settings</i> Restaurant
        </a>

        <Dialog
          title="Your restaurant"
          actions={actions}
          modal={true}
          contentStyle={customContentStyle}
          open={this.state.open}
          autoScrollBodyContent={true}
        >
          <div className={css(styles.sections)}>
            <div>
              <TextField
                hintText="Enter your restaurant name"
                floatingLabelText="Restaurant name"
                floatingLabelFixed={true}
                fullWidth
                value={this.state.form.NameRestaurant}
                onChange={(e, value) => this.onChangeForm('NameRestaurant', value)}
              />
              <TextField
                hintText="Enter your restaurant address"
                floatingLabelText="Restaurant address (Optional)"
                floatingLabelFixed={true}
                fullWidth
                value={this.state.form.AdressRestaurant}
                onChange={(e, value) => this.onChangeForm('AdressRestaurant', value)}
              />
              <TextField
                hintText="Enter your restaurant info"
                floatingLabelText="Restaurant info"
                floatingLabelFixed={true}
                fullWidth
                value={this.state.form.InfoRestaurant}
                onChange={(e, value) => this.onChangeForm('InfoRestaurant', value)}
                multiLine={true}
                rows={3}
                rowsMax={6}
              />
            </div>
            <div>
              <div className={css(styles.noPhoto)}>
                {this.state.form.Photo !== '' && this.state.form.Photo !== null ? (
                  <img src={fileActions.restaurantPhoto(this.state.form.Photo)} alt="category logo" className={css(styles.categoryImage)} />
                ) : (
                  <div>Upload image of category</div>
                )}
                <FlatButton
                  label="Upload"
                  secondary
                  icon={<i className="material-icons">file_upload</i>}
                  className={css(styles.uploadBtn)}
                >
                  <input type="file" className={css(styles.uploadInput)} onChange={e => this.onChangeImage(e)} ref={node => this.fileInput = node} />
                </FlatButton>
              </div>
            </div>
          </div>
        </Dialog>
      </li>
    )
  }

}

const styles = StyleSheet.create({
  sections: {
    display: 'flex',
    ':nth-child(1n) > div': {
      flex: 1,
    },
    ':nth-child(1n) > div:last-child': {
      borderLeft: '1px solid #e0e0e0',
      marginLeft: 15,
      paddingLeft: 15,
      display: 'flex',
      justifyContent: 'center'
    }
  },

  noPhoto: {
    alignSelf: 'center'
  },

  uploadBtn: {
    margin: '10px auto 0 auto !important',
    display: 'block !important',
    position: 'relative !important',
    verticalAlign: 'middle !important',
  },

  uploadInput: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    width: '100%',
    opacity: 0,
    zIndex: 2,
    cursor: 'pointer !important'
  },

  categoryImage: {
    maxWidth: '100%',
    background: '#f1f1f1',
    display: 'block',
    margin: '0 auto',
  }
})

export default connect(state => ({ user: state.user, restaurant: state.restaurant }))(RestaurantDialog)