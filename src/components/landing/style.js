import { StyleSheet } from 'aphrodite'

export const buttons = StyleSheet.create({
  green: {
    background: 'rgba(1, 177, 175, 1)',
    border: 'solid rgba(0, 108, 109, 1) 0px',
    borderRadius: 3,
    cursor: 'pointer',
    minWidth: 160,
    color: '#fff',
    fontFamily: 'Raleway, sans-serif',
    fontSize: 14,
    fontWeight: 'bold',
    padding: 9,
    transition: '0.4s ease',
    ':hover': {
      background: 'rgba(0, 108, 109, 1)',
      transition: '0.4s ease'
    },
    ':focus': {
      outline: 0
    }
  },

  yellow: {
    background: '#f05273'
  },

  autoWidth: {
    minWidth: 'auto'
  }
})