const callback = (name, company, phone, email = "no specified") => ({
  "Title": "Foodsotory schedule callback",
  "Body": `
    <p>Callback request</p>
     
    <p>
      Fullname: ${name}<br />
      Company: ${company}<br />
      Phone: ${phone}<br />
      Email: ${email}
    </p>
  `
})


export default callback;