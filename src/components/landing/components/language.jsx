import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite'
import { setLanguage } from 'react-redux-multilang'
import { connect } from 'react-redux'

import enFlag from '../../../sources/img/en.png'
import nlFlag from '../../../sources/img/nl.png'

class Language extends Component{

  onSelectLanguage(code){
    setLanguage(code)
    localStorage.setItem('language', code)
  }

  render(){
    return (
      <section className={css(styles.languages)}>
        <img src={enFlag} alt="English" onClick={() => this.onSelectLanguage('en')} className={this.props.lang.code === 'en' ? 'active' : ''} />
        <img src={nlFlag} alt="Niderland" onClick={() => this.onSelectLanguage('nl')} className={this.props.lang.code === 'nl' ? 'active' : ''} />
      </section>
    )
  }

}

const styles = StyleSheet.create({
  languages: {
    position: 'fixed',
    zIndex: 10,
    right: 15,
    top: 67,
    background: '#fff',
    padding: '0 5px',
    border: '2px solid #f05272',
    borderRadius: 5,
    height: 24,
    ':nth-child(1n) > img': {
      opacity: 0.5,
      cursor: 'pointer',
    },
    ':nth-child(1n) > img.active': {
      opacity: 1
    },
    ':nth-child(1n) > img:first-child': {
      marginRight: 5
    },
    '@media (max-width: 450px)': {
      top: 52,
      right: 'calc(50% - 90px)'
    }
  }
})

export default connect(state => ({ lang: state.lang }))(Language)