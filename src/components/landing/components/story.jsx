import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite'
import scrollToElement from 'scroll-to-element'
import MultiLanguage from 'react-redux-multilang'
import { connect } from 'react-redux'

import storyleft from '../../../sources/img/storyleft.jpg'
import storyright from '../../../sources/img/storyright.jpeg'

import { buttons } from '../style'
import CallbackModal from './callbackModal'

class Story extends Component{

  render(){
    return (
      <div className={css(styles.wrapper)}>
        <div className={css(styles.buttons)}>
          <button onClick={() => scrollToElement('#callback', { offset: -80 })} className={css(buttons.green, buttons.yellow)}>{translate.getstart}</button>
          <span>{translate.or}</span>
          <CallbackModal text={translate.callback} />
        </div>

        <h3 className={css(styles.title)}>{translate.title1}<br />{translate.title2}</h3>
        <div className={css(styles.line)}></div>

        <div className={css(styles.four)}>
          <div style={{ background: 'url('+ storyleft +')' }}></div>
          <div>
            <strong>{translate.amaze1}<br />{translate.amaze2}</strong>
            <div className={css(styles.line)}></div>
            <p>{translate.amazetext}</p>
          </div>
          <div>
            <strong>{translate.story}</strong>
            <div className={css(styles.line)}></div>
            <p>{translate.storytext}</p>
          </div>
          <div style={{ background: 'url('+ storyright +')' }}></div>
        </div>

        <div className={css(styles.buttons)}>
          <button onClick={() => scrollToElement('#callback', { offset: -80 })} className={css(buttons.green, buttons.yellow)}>{translate.getstart}</button>
          <span>{translate.or}</span>
          <CallbackModal text={translate.callback} />
        </div>

        <h3 className={css(styles.title, styles.titleTop)}>{translate.titlebot1}<br />{translate.titlebot2}<br />{translate.titlebot3}</h3>
        <div className={css(styles.line)}></div>
      </div>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    callback: 'Call me back please!',
    getstart: 'Let\'s get started!',
    or: 'or',
    title1: 'Food & Innovation go hand in hand. Augmented',
    title2: 'Reality in Food is the future.',
    amaze1: 'Amaze your',
    amaze2: 'customers',
    amazetext: 'Staying on top of your game means to constantly surprise and amaze your customers. Allowing customers to view their choices in 3D first will literally blow them away.',
    story: 'Tell your story & sell more',
    storytext: 'Choosing food is an emotional choice driven by stunning presentations and visuals. With your menu in 3D you can dictate the customer emotion.',
    titlebot1: 'Using Augmented Reality for the Food menu and',
    titlebot2: 'presentations is suited for Restaurants, Hotels,',
    titlebot3: 'Catering and Food Retailers.',
  },
  nl: {
    callback: 'Bel mij aub terug!',
    getstart: 'Ik wil graag starten!',
    or: 'of',
    title1: 'Food & Innovatie gaan hand in hand. Augmented ',
    title2: 'Reality in Food is de toekomst.',
    amaze1: 'Blijvende ervaring',
    amaze2: 'voor klanten',
    amazetext: 'Concurreren in de horeca betekent constant innoveren en je klanten blijven verbazen. Klanten die gerechten in 3D kunnen bekijken zullen zonder twijfel een geweldige ervaring meekrijgen.',
    story: 'Vertel jouw verhaal en verkoop meer',
    storytext: 'Een gerecht kiezen is een emotionele keuze beïnvloed door visuele presentatie. Met het 3D menu kun je als restaurant jouw menu sterker neerzetten en meer verkopen.',
    titlebot1: 'Het gebruiken van Augmented Reality voor een 3D ',
    titlebot2: 'Menu is geschikt voor Restaurants, Hotels, ',
    titlebot3: 'Cateraars en Food Retailers.',
  }
})

const styles = StyleSheet.create({
  wrapper: {
    position: 'relative',
    background: '#fff',
    overflow: 'auto',
    padding: '50px 0',
  },

  line: {
    display: 'block',
    width: 100,
    borderTop: '1px solid #ccc',
    margin: '25px auto',
  },

  buttons: {
    marginTop: 50,
    display: 'flex',
    width: 500,
    margin: '0 auto',
    justifyContent: 'space-between',
    ':nth-child(1n) > span': {
      fontWeight: 'bold',
      paddingTop: 5,
      margin: '0 15px',
      '@media (max-width: 480px)': {
        padding: '15px 0',
        textAlign: 'center',
      }
    },
    ':nth-child(1n) > button': {
      flex: 1
    },
    '@media (max-width: 530px)': {
      width: 'calc(100% - 30px)',
      margin: '0 15px'
    },
    '@media (max-width: 480px)': {
      flexDirection: 'column',
    }
  },

  title:{
    marginTop: 75,
    fontSize: 25,
    textAlign: 'center',
    '@media (max-width: 480px)': {
      fontSize: 20
    }
  },
  titleTop: {
    marginTop: 150,
    '@media (max-width: 480px)': {
      fontSize: 18
    }
  },

  four: {
    display: 'flex',
    flexDirection: 'row',
    margin: '50px 0',
    '@media (max-width: 480px)': {
      flexDirection: 'column',
    },
    ':nth-child(1n) > div': {
      flex: 1,
      textAlign: 'center',
      marginRight: 15,
      padding: '75px 20px',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      ':nth-child(1n) > p': {
        lineHeight: '1.6em',
        fontSize: 14
      },
      ':nth-child(1n) > strong': {
        letterSpacing: '0.05em',
        fontSize: 20,
        color: 'red'
      },
      '@media (max-width: 950px)': {
        flex: 3
      },
      '@media (max-width: 480px)': {
        padding: '25px 20px',
      }
    },
    ':nth-child(1n) > div:last-child': {
      marginRight: 0,
      '@media (max-width: 950px)': {
        flex: 1
      },
      '@media (max-width: 680px)': {
        display: 'none',
      }
    },
    ':nth-child(1n) > div:first-child': {
      '@media (max-width: 950px)': {
        flex: 1
      },
      '@media (max-width: 680px)': {
        display: 'none',
      }
    }
  }
})

export default connect(state => ({ lang: state.lang }))(Story)