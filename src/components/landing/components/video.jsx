import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite'
import MultiLanguage from 'react-redux-multilang'
import { connect } from 'react-redux'

import { buttons } from '../style'

import videoMp4 from '../../../sources/img/bg.mp4'
import videoWebm from '../../../sources/img/bg.webm'
import noVideo from '../../../sources/img/lefttop1.jpg'

class Video extends Component{

  state = {
    open: false
  }

  onClickOpen(){
    this.setState({ open: true })
  }

  onClickClose(){
    this.setState({ open: false })
  }

  render(){
    return (
      <div className={css(styles.wrapper)}>
        <button onClick={() => this.onClickOpen()} className={css(buttons.green, styles.button)}><i className="material-icons">play_circle_filled</i> {translate.play}</button>
        { this.state.open && (
          <div className={css(styles.fullScreen)}>
            <video controls playsInline>
              <source type="video/mp4" src={videoMp4} />
              <source type="video/webm" src={videoWebm} />
              <img src={noVideo} alt="no-video" />
            </video>
            <i className="material-icons" onClick={() => this.onClickClose()}>close</i>
          </div>
        ) }
      </div>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    play: 'Watch the video',
  },
  nl: {
    play: 'Bekijk de video',
  }
})

const styles = StyleSheet.create({
  wrapper: {
    position: 'relative',
    background: 'transparent',
    overflow: 'auto',
    minHeight: 500
  },

  button: {
    position: 'absolute',
    left: 'calc(50% - 80px)',
    top: 'calc(50% - 10px)',
    ':nth-child(1n) > i': {
      margin: '-11px 5px 0 0',
      position: 'relative',
      top: 6
    }
  },

  fullScreen: {
    background: '#333',
    position: 'fixed',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    zIndex: 10000,
    ':nth-child(1n) > i': {
      position: 'absolute',
      right: 15,
      top: 15,
      fontSize: 30,
      cursor: 'pointer',
      color: '#fff',
      textShadow: '0px 0px 5px #000'
    },
    ':nth-child(1n) > video': {
      width: '100%',
      height: '100%',
      margin: 'auto auto'
    }
  }
})

export default connect(state => ({ lang: state.lang }))(Video)