import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'
import { Link } from 'react-router-dom'
import { HashLink } from 'react-router-hash-link'
import scrollToElement from 'scroll-to-element'
import MultiLanguage from 'react-redux-multilang'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'

import arbi from '../../../sources/img/arbi.png'
// import fb from '../../../sources/img/fb.png'
// import tw from '../../../sources/img/tw.png'
// import gP from '../../../sources/img/g+.png'
// import yt from '../../../sources/img/yt.png'
// import pt from '../../../sources/img/pt.png'
// import ig from '../../../sources/img/ig.png'

// import { buttons } from '../style'
// import CallbackModal from './callbackModal'

class Footer extends Component{

  render(){
    return (
      <div className={css(styles.wrapper)}>
        <div>
          <div>
            {translate.desc1} <a className={css(styles.link)} href="https://arbi.io/">{translate.desc2}</a> {translate.desc3}
            <img className={css(styles.logo)} src={arbi} alt="Arbi logo" />
          </div>
          <div>
            <strong className={css(styles.countries)}>{translate.countries}</strong>
            <p className={css(styles.country)}>London (United Kingdom)</p>
            <p className={css(styles.country)}>Mumbai (India)</p>
            <p className={css(styles.country)}>Singapore (Singapore)</p>
          </div>
          <div>
            <ul className={css(styles.list)}>
              <li><Link to="/login">Login</Link></li>
              <li>
                { this.props.match.path === '/' ? (
                  <a href="" onClick={(e) => { e.preventDefault(); scrollToElement('#callback', { offset: -80 })}}>{translate.contactus}</a>
                ) : (
                  <HashLink to="/#callback">{translate.contactus}</HashLink>
                ) }
              </li>
            </ul>
          </div>
          <div>
            <ul className={css(styles.list)}>
              <li><a href="https://arbi.io/">{translate.corporate}</a></li>
              <li><a href="https://arbi.io/designers">{translate.designers}</a></li>
            </ul>
          </div>
        </div>
      </div>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    desc1: 'FoodStory is a product of Arbi and powered by the Arbi platform. for more information on Arbi,',
    desc2: 'visit',
    desc3: 'our website',
    countries: 'Foodstory is also available in:',
    contactus: 'Contact us',
    corporate: 'For corporates',
    designers: 'For designers'
  },
  nl: {
    desc1: 'FoodStory is een product van Arbi en mogelijk gemaakt door het Arbi platform. Voor meer informatie over Arbi,',
    desc2: 'bezoek',
    desc3: 'onze website.',
    countries: 'Foodstory is ook beschikbaar in:',
    contactus: 'Contact',
    corporate: 'Voor bedrijven',
    designers: 'Voor designers'
  }
})

const styles = StyleSheet.create({
  wrapper: {
    position: 'relative',
    background: '#43b1af',
    overflow: 'auto',
    ':nth-child(1n) > div': {
      width: 900,
      margin: '50px auto',
      color: '#fff',
      display: 'flex',
      fontSize: 14,
      ':nth-child(1n) > div': {
        flex: 1,
        padding: 15,
        '@media (max-width:800px)': {
          width: '50%',
          display: 'block',
          boxSizing: 'border-box',
          flex: 'initial !important',
        },
        '@media (max-width:500px)': {
          width: '100%',
        }
      },
      ':nth-child(1n) > div:nth-child(1)': {
        flex: '2 !important',
        '@media (max-width:800px)': {
          flex: 'initial !important',
        }
      },
      ':nth-child(1n) > div:nth-child(2)': {
        flex: '2 !important',
        '@media (max-width:800px)': {
          flex: 'initial !important',
        }
      },
      '@media (max-width:930px)': {
        width: 'calc(100% - 30px)',
        margin: '50px 15px'
      },
      '@media (max-width:800px)': {
        flexWrap: 'wrap',
      }
    }
  },

  logo: {
    margin: '0 auto',
    display: 'table',
    marginTop: 30
  },

  countries:{
    display: 'block',
    marginBottom: 15
  },

  country: {
    marginBottom: 5,
    marginTop: 0
  },

  link: {
    color: '#fff'
  },

  list: {
    listStyle: 'none',
    margin: 0,
    padding: 0,
    ':nth-child(1n) > li > a': {
      color: '#fff',
      display: 'block',
      marginBottom: 5,
      textDecoration: 'none',
      ':hover':{
        textDecoration: 'underline',
      }
    }
  }
})

export default connect(state => ({ lang: state.lang }))(withRouter(Footer))