import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite'
import { toast as NotificationManager } from 'react-toastify'
import MultiLanguage from 'react-redux-multilang'
import { connect } from 'react-redux'
import Modal from 'react-modal'
import countriesList from 'countries-list'

import { buttons } from '../style'

import landingCallback from '../mails/callback'
import * as sendMessage from '../../../actions/sendMessage'

// eslint-disable-next-line
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const numbersRegex = /^([^0-9]*)$/
// eslint-disable-next-line
const phoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im

let defaultState = {
  isOpen: false,
  callback: {
    fullname: '',
    company: '-',
    code: '',
    phone: '',
    email: ''
  },
}

class CallbackModal extends Component {

  state = { ...defaultState }

  componentWillMount() {
    Modal.setAppElement('body');
    this.requestUserCountry()
  }

  async onSubmit(e) {
    e.preventDefault();

    try {
      this.validator();

      let mail = landingCallback(
        this.state.callback.fullname,
        this.state.callback.company,
        this.state.callback.code + this.state.callback.phone,
        this.state.callback.email);
      await sendMessage.createMail(this.state.callback.email, mail);

      NotificationManager.success(translate.success);
      this.setState({ ...defaultState });
    } catch (e) {
      console.log(e)
      try {
        // eslint-disable-next-line
        e = JSON.parse(e)
      } catch (e) { }
      NotificationManager.error(e.Message || e.message || e);
    }
  }

  validator() {
    if (this.state.callback.fullname === '') throw new Error(translate.fullnameReq);
    if (this.state.callback.company === '') throw new Error(translate.companyReq);
    if (this.state.callback.phone === '') throw new Error(translate.phoneReq);
    if (this.state.callback.email === '') throw new Error(translate.emailReq);

    if (!numbersRegex.test(this.state.callback.fullname)) throw new Error(translate.fullnameInvalid);
    if (!emailRegex.test(this.state.callback.email)) throw new Error(translate.emailInvalid);
    if (!phoneRegex.test(this.state.callback.code + this.state.callback.phone)) throw new Error(translate.phoneInvalid);
  }

  requestClose(e){
    if(e) e.stopPropagation();
    this.setState({ isOpen: false })
  }

  requestOpen() {
    this.setState({ isOpen: true })
  }

  onChangeCallback(field, value){
    this.setState({
      callback: {
        ...this.state.callback,
        [field]: value
      }
    })
  }

  async requestUserCountry(){
    let response = await fetch('https://ipinfo.io/json')
    if(response.status === 200){
      let data = await response.json()

      let phoneCode = countriesList.countries[data.country].phone.split(',')

      if(data.country){
        this.setState({
          callback: {
            ...this.state.callback,
            code: '+' + phoneCode[0]
          }
        })
        defaultState.callback.code = '+' + phoneCode[0] 
      }
    }
  }

  render() {
    
    let countryCodes = Object.keys(countriesList.countries)
    const phoneCodes = countryCodes.map((countryCode, i) => {
      let phoneCode = countriesList.countries[countryCode].phone.split(',')
      return (<option key={i} value={`+${phoneCode[0]}`}>{`+${phoneCode[0]}`}</option>)
    })

    return (
      <button className={css(buttons.green)} onClick={() => this.requestOpen()}>
        {this.props.text}
      
        <Modal
          isOpen={this.state.isOpen}
          onRequestClose={(e) => this.requestClose(e)}
          contentLabel="Modal"
          className={{
            base: css(styles.modal)
          }}
          overlayClassName={{
            base: css(styles.modalOverlay)
          }}
        >
          <p className={css(styles.text)}>{translate.text}</p>
          <div className={css(styles.line)}></div>
          <i onClick={e => this.requestClose(e)} className={css(styles.close) + " material-icons"}>close</i>
          
          <form onSubmit={e => this.onSubmit(e)} className={css(styles.form)}>

            <input
              value={this.state.callback.fullname}
              onChange={e => this.onChangeCallback('fullname', e.target.value)}
              placeholder={translate.fullname}
              className={css(styles.input)} />

            <div className={css(styles.phoneAndCode)}>
              <select 
                value={this.state.callback.code} 
                className={css(styles.input)}
                onChange={e => this.onChangeCallback('code', e.target.value)}>
                {phoneCodes}
              </select>
              <input
                value={this.state.callback.phone}
                onChange={e => this.onChangeCallback('phone', e.target.value)}
                placeholder={translate.phone}
                className={css(styles.input)} />
            </div>

            <input
              value={this.state.callback.email}
              onChange={e => this.onChangeCallback('email', e.target.value)}
              placeholder={translate.email}
              className={css(styles.input)} />

          </form>
          <div className={css(styles.controls)}>
            <button className={css(buttons.green)} onClick={e => this.onSubmit(e)}>{translate.send}</button>
          </div>
        </Modal>
      </button>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    text: 'Sometimes a call is just easier. Leave your info and we will call you back asap!',
    fullname: 'Full name',
    email: 'Your workemail',
    phone: 'Phone number incl. landcode',
    send: 'Send callback request',
    success: 'Thank you for your message. We will reply within 24 hours or less. Promised!',
    fullnameReq: 'Fullname is required field.',
    companyReq: 'Company is required field.',
    phoneReq: 'Phone is required field.',
    emailReq: 'Email is required field.',
    fullnameInvalid: 'Full name field is invalid.',
    phoneInvalid: 'Phone number field is invalid.',
    emailInvalid: 'Email field is invalid.'
  },
  nl: {
    text: 'Een belletje is makkelijker. Laat je informatie achter en wij nemen met jou contact op.',
    fullname: 'Volledige naam',
    email: 'Jouw bedrijfsemail',
    phone: 'Telefoon nummer',
    send: 'Verzend verzoek terugbelafspraak',
    success: 'Bedankt voor je bericht. We antwoorden binnen 24 uur of minder. Beloofd!',
    fullnameReq: 'Volledige naam is verplicht veld.',
    companyReq: 'Bedrijf is verplicht veld.',
    phoneReq: 'Telefoon is verplicht veld.',
    emailReq: 'E-mail is verplicht veld.',
    fullnameInvalid: 'Het veld Volledige naam is ongeldig.',
    phoneInvalid: 'Telefoonnummer veld is ongeldig.',
    emailInvalid: 'Het e-mailveld is ongeldig.'
  }
})

const styles = StyleSheet.create({
  phoneAndCode: {
    display: 'flex',
    flexDirection: 'row',
    ':nth-child(1n) > input': {
      flex: 3,
      minWidth: 0
    },
    ':nth-child(1n) > select': {
      flex: 1,
      minWidth: 0
    }
  },

  modalOverlay: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    overflow: 'auto',
    zIndex: 100,
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },

  modal: {
    left: 'initial',
    right: 'initial',
    width: 500,
    margin: '50px auto',
    position: 'relative',
    padding: '20px 75px',
    background: '#fff',
    border: '5px solid #256c6d',
    outline: 0,
    '@media (max-width:700px)': {
      width: 'calc(100% - 180px)',
      marginLeft: 10,
      marginRight: 10
    },
    '@media (max-width:500px)': {
      width: 'calc(100% - 60px)',
      padding: '20px 15px',
    }
  },

  form: {
    display: 'flex',
    flexDirection: 'column',
  },

  input: {
    border: 0,
    margin: 10,
    borderRadius: 0,
    fontSize: 14,
    color: '#000',
    background: 'rgba(231,231,231,1)',
    padding: 10,
    ':focus': {
      outline: 0,
      border: 0,
      background: 'rgba(231,231,231,1)',
    },
    ':active': {
      outline: 0,
      border: 0,
      background: 'rgba(231,231,231,1)',
    }
  },

  controls: {
    marginTop: 5,
    display: 'flex',
    padding: 10,
    ':nth-child(1n) > button': {
      flex: 1,
      marginRight: 5
    },
    ':nth-child(1n) > button:last-child': {
      marginRight: 0
    }
  },
  
  title: {
    padding: 10,
    fontFamily: 'Raleway, sans-serif',
    borderBottom: '1px solid #c1c1c1',
    margin: 0
  },

  text: {
    color: '#f05273',
    fontSize: 20,
    textAlign: 'center',
    fontWeight: 'bold',
  },

  line: {
    borderTop: '1px solid #ccc',
    width: '80%',
    display: 'block',
    margin: '20px auto',
  },

  close: {
    position: 'absolute',
    right: 5,
    top: 5,
    cursor: 'pointer',
  }
})

export default connect(state => ({ lang: state.lang }))(CallbackModal)