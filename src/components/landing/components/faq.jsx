import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite'
import MultiLanguage from 'react-redux-multilang'
import { connect } from 'react-redux'

class Faq extends Component{

  render(){
    return (
      <div className={css(styles.wrapper)}>

        <h2 className={css(styles.faq)}>FAQ</h2>

        <div>
          <div>
            <h4>{translate.question1}</h4>
            <div className={css(styles.line)}></div>
            <p>{translate.answer1}</p>
          </div>
          <div>
            <h4>{translate.question2}</h4>
            <div className={css(styles.line)}></div>
            <p>{translate.answer2}</p>
          </div>
          <div>
            <h4>{translate.question3}</h4>
            <div className={css(styles.line)}></div>
            <p>{translate.answer3}</p>
          </div>
          <div>
            <h4>{translate.question4}</h4>
            <div className={css(styles.line)}></div>
            <p>{translate.answer4}</p>
          </div>
        </div>
      </div>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    question1: 'How does Foodstory works for my restaurant?',
    answer1: 'First, we take 360-degree photographs of your dishes which are then converted into 3D models. These are loaded into our system. Once ready, you receive a notification.',
    question2: 'What is Augmented Reality exactly?',
    answer2: 'If you or your kids have played Pokemon Go, you have seen Pokemons running around your kitchen through your phone camera. That\'s Augmented Reality. Projecting 3D models through a phone camera into the real world.',
    question3: 'How can my customers see the 3D menu?',
    answer3: 'Customers download the Foodstory app on IOS or Android and view the menu. The restaurant can also choose to use Tablets and share these with customers.',
    question4: 'Will the 3D food models look good?',
    answer4: 'We combine photography with special software which allows the 3D food models to be extremely food-realistic, just like the real deal.',
  },
  nl: {
    question1: 'How does Foodstory works for my restaurant?',
    answer1: 'First, we take 360-degree photographs of your dishes which are then converted into 3D models. These are loaded into our system. Once ready, you receive a notification.',
    question2: 'What is Augmented Reality exactly?',
    answer2: 'If you or your kids have played Pokemon Go, you have seen Pokemons running around your kitchen through your phone camera. That\'s Augmented Reality. Projecting 3D models through a phone camera into the real world.',
    question3: 'How can my customers see the 3D menu?',
    answer3: 'Customers download the Foodstory app on IOS or Android and view the menu. The restaurant can also choose to use Tablets and share these with customers.',
    question4: 'Will the 3D food models look good?',
    answer4: 'We combine photography with special software which allows the 3D food models to be extremely food-realistic, just like the real deal.',
  }
})

const styles = StyleSheet.create({
  wrapper: {
    position: 'relative',
    background: '#fff',
    overflow: 'auto',
    ':nth-child(1n) > div': {
      width: 900,
      margin: '50px auto',
      display: 'flex',
      flexWrap: 'wrap',
      ':nth-child(1n) > div': {
        width: '35%',
        marginRight: '30%',
        ':nth-child(1n) > h4': {
          color: '#FF6161',
          fontWeight: 'bold',
        },
        ':nth-child(1n) > p': {
          fontSize: 14
        },
        '@media (max-width:930px)': {
          width: '100%'
        }
      },
      ':nth-child(1n) > div:nth-child(2n)': {
        marginRight: 0
      },
      '@media (max-width:930px)': {
        width: 'calc(100% - 30px)',
        margin: '50px 15px'
      }
    }
  },

  line: {
    display: 'block',
    width: 100,
    borderTop: '1px solid #ccc',
    margin: '15px 0',
  },

  faq: {
    textAlign: 'center',
    fontSize: 35,
    paddingBottom: 10,
    marginBottom: 10,
    position: 'relative',
    ':after': {
      content: '""',
      display: 'block',
      position: 'absolute',
      bottom: 0,
      borderTop: '1px solid #ccc',
      width: 50,
      left: 'calc(50% - 25px)'
    }
  }
})

export default connect(state => ({ lang: state.lang }))(Faq)