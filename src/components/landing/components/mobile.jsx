import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite'
import MultiLanguage from 'react-redux-multilang'
import { connect } from 'react-redux'

//import mobile from '../../../sources/img/mobile.webp'
import iphone from '../../../sources/img/iphone.png'
import googlePlay from '../../../sources/img/googleplay.png'
import appStore from '../../../sources/img/appstore.png'

// import { buttons } from '../style'

class Mobile extends Component{

  render(){
    return (
      <div className={css(styles.wrapper)}>
        <div>
          <img src={iphone} alt="mobile" />
          <div>
            <h3>{translate.text}</h3>
            <div className={css(styles.line)}></div>
            <div className={css(styles.links)}>
              <a href="" target="_blank"><img src={googlePlay} alt="Get it on google play" /></a>
              <a href="" target="_blank"><img src={appStore} alt="Get it on app store" /></a>
            </div>
          </div>
        </div>
      </div>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    text: 'The 3D Augmented Reality menu is used with the Mobile Foodstory App, available on IOS & Android.',
  },
  nl: {
    text: 'Het 3D Menu kan bekeken worden met de Foodstory app, beschikbaar op IOS en Android.',
  }
})

const styles = StyleSheet.create({
  wrapper: {
    position: 'relative',
    background: '#fff',
    overflow: 'auto',
    ':nth-child(1n) > div': {
      width: 550,
      margin: '50px auto',
      display: 'flex',
      flexDirection: 'column',
      ':nth-child(1n) > div': {
        flex: 1,
        paddingTop: 50,
        '@media (max-width:700px)': {
          paddingTop: 0
        }
      },
      ':nth-child(1n) > img': {
        width: '100%',
        '@media (max-width:700px)': {
          //display: 'none',
        },
        '@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none)': {
          height: 240
        }
      },
      '@media (max-width:930px)': {
        width: 'calc(100% - 30px)',
        margin: '0 15px'
      }
    },
    '@media screen and (-ms-high-contrast: active), (-ms-high-contrast: none)': {
      minHeight: 575
    }
  },

  line: {
    display: 'block',
    width: 100,
    borderTop: '1px solid #ccc',
    margin: '25px 0',
  },

  links: {
    display: 'flex',
    justifyContent: 'space-around',
    '@media (max-width:870px)': {
      flexWrap: 'wrap',
    },
    ':nth-child(1n) > a': {
      '@media (max-width:870px)': {
        marginBottom: 15
      }
    }
  }
})

export default connect(state => ({ lang: state.lang }))(Mobile)