import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite'
import scrollToElement from 'scroll-to-element'
import MultiLanguage from 'react-redux-multilang'
import { connect } from 'react-redux'

// import right1 from '../../../sources/img/right1.jpeg'

import { buttons } from '../style'
import CallbackModal from './callbackModal'

class Why extends Component{

  render(){
    return (
      <div className={css(styles.wrapper)}>
        <div>
          <h3>{translate.title}</h3>
          <div className={css(styles.line)}></div>
          <p>{translate.line1}</p> ​
          <p>{translate.line2}</p>
          <p>{translate.line3}</p>
          <div className={css(styles.buttons)}>
            <button onClick={() => scrollToElement('#callback', { offset: -80 })} className={css(buttons.green, buttons.yellow)}>{translate.getstart}</button>
            <span>{translate.or}</span>
            <CallbackModal text={translate.callback} />
          </div>
        </div>
      </div>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    title: 'Why Foodstory?',
    callback: 'Call me back please!',
    getstart: 'Let\'s get started!',
    line1: 'Creating food is an amazing story to tell. The ingredients, the preparation, the chef\'s passion, the philosophy, the secret touches and the love put in.',
    line2: 'People don\'t simply want food. They want an experience and they want to be amazed.',
    line3: 'With Augmented Reality, you can now visualize that story with a 3D food menu that brings your creations virtually to the table.',
    or: 'or'
  },
  nl: {
    title: 'Waarom Foodstory?',
    callback: 'Bel mij aub terug!',
    getstart: 'Ik wil graag starten!',
    line1: 'Elke culinaire creatie heeft een eigen uniek verhaal. De ingrediënten, de voorbereidingstechniek, de passie van de chef, de filosofie en de liefde die in elk gerecht gaat.',
    line2: 'Klanten willen niet alleen heerlijk eten. Zij willen een unieke en blijvende ervaring. ',
    line3: 'Met Augmented Reality is het nu mogelijk om jouw verhaal te visualiseren met het 3D Food Menu waarmee klanten gerechten virtueel op tafel kunnen bekijken. ',
    or: 'of'
  }
})


const styles = StyleSheet.create({
  wrapper: {
    position: 'relative',
    background: '#fff',
    overflow: 'auto',
    ':nth-child(1n) > div': {
      width: 500,
      margin: '50px auto',
      ':nth-child(1n) > h3': {
        //fontFamily: 'Arial,Helvetica,sans-serif',
        fontSize: 30,
        textAlign: 'center',
      },
      '@media (max-width: 530px)': {
        width: 'calc(100% - 30px)'
      }
    }
  },

  line: {
    display: 'block',
    width: 100,
    borderTop: '1px solid #ccc',
    margin: '25px auto',
  },

  buttons: {
    display: 'flex',
    justifyContent: 'space-between',
    ':nth-child(1n) > span': {
      fontWeight: 'bold',
      paddingTop: 5,
      margin: '0 15px',
      '@media (max-width: 480px)': {
        padding: '15px 0',
        textAlign: 'center',
      }
    },
    ':nth-child(1n) > button': {
      flex: 1
    },
    '@media (max-width: 480px)': {
      flexDirection: 'column',
    }
  }
})

export default connect(state => ({ lang: state.lang }))(Why)