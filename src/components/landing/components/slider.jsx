import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite'
import MultiLanguage from 'react-redux-multilang'
import { connect } from 'react-redux'

import four1 from '../../../sources/img/four1.jpg'
import four2 from '../../../sources/img/four2.jpeg'
import four3 from '../../../sources/img/four3.jpeg'
import four4 from '../../../sources/img/four4.jpeg'

import { buttons } from '../style'

class Slider extends Component{

  render(){
    return (
      <div className={css(styles.wrapper)}>
        <div>
          <div>
            <button className={css(buttons.green, buttons.yellow)}>{translate.resttitle}</button>
            <p>{translate.restdesc}</p>
          </div>
        </div>
        <div>
          <div>
            <button className={css(buttons.green, buttons.yellow)}>{translate.hotelstitle}</button>
            <p>{translate.hotelsdesc}</p>
          </div>
        </div>
        <div>
          <div>
            <button className={css(buttons.green, buttons.yellow)}>{translate.catertitle}</button>
            <p>{translate.caterdesc}</p>
          </div>
        </div>
        <div>
          <div>
            <button className={css(buttons.green, buttons.yellow)}>{translate.retailtitle}</button>
            <p>{translate.retaildesc}</p>
          </div>
        </div>
      </div>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    resttitle: 'Restaurants',
    restdesc: 'Lets your customers view the dishes in 3D holograms.',
    hotelstitle: 'Hotels',
    hotelsdesc: 'Use the 3D menu for the restaurants, roomservice & events.',
    catertitle: 'Caterers',
    caterdesc: 'Compile a menu or offer to customers using the 3D menu in a more powerful way.',
    retailtitle: 'Food retailers',
    retaildesc: 'Use the 3D food visualization for marketing campaigns, tastings and product introductions.'
  },
  nl: {
    resttitle: 'Restaurants',
    restdesc: 'Laat jouw klanten gerechten in 3D bekijken waarbij zij het eten virtueel kunnen ervaren.',
    hotelstitle: 'Hotels',
    hotelsdesc: 'Gebruik het 3D menu voor het restaurant, room-service of bij evenementen.',
    catertitle: 'Caterers',
    caterdesc: 'Stel een menu samen voor je klanten door hen het aanbod in 3D te laten bekijken.',
    retailtitle: 'Food retailers',
    retaildesc: 'Gebruik het 3D Food Menu voor marketingcampagnes, proeverijen of tijdens evenementen.'
  }
})

const styles = StyleSheet.create({
  wrapper: {
    position: 'relative',
    background: '#fff',
    overflow: 'auto',
    padding: '25px 0',
    display: 'flex',
    flexDirection: 'row',
    '@media (max-width: 975px)': {
      flexWrap: 'wrap',
    },
    ':nth-child(1n) > div': {
      flex: 1,
      textAlign: 'center',
      padding: 15,
      minHeight: 300,
      paddingTop: 150,
      position: 'relative',
      ':after': {
        content: '""',
        display: 'block',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        background: 'rgba(255,255,255, 0.5)',
        zIndex: 2
      },
      ':nth-child(1n) > div': {
        zIndex: 3,
        position: 'relative',
        color: '#000',
        fontWeight: 'bold',
        padding: '0 25px'
      },
      '@media (max-width: 975px)': {
        width: '50%',
        flex: 'initial',
        boxSizing: 'border-box',
      },
      '@media (max-width: 480px)': {
        width: '100%'
      }
    },
    ':nth-child(1n) > div:nth-child(1)': {
      background: 'url(' + four1 + ')',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    ':nth-child(1n) > div:nth-child(2)': {
      background: 'url(' + four2 + ')',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    ':nth-child(1n) > div:nth-child(3)': {
      background: 'url(' + four3 + ')',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    },
    ':nth-child(1n) > div:nth-child(4)': {
      background: 'url(' + four4 + ')',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
    }
  },

})

export default connect(state => ({ lang: state.lang }))(Slider)