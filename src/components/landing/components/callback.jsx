import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite'
import { toast as NotificationManager } from 'react-toastify';
import MultiLanguage from 'react-redux-multilang'
import { connect } from 'react-redux'

// import { buttons } from '../style'
import CallbackModal from './callbackModal'

import contact from '../mails/contact'
import * as sendMessage from '../../../actions/sendMessage'

// eslint-disable-next-line
const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const numbersRegex = /^([^0-9]*)$/
const phoneRegex = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/

const defaultState = {
  name: '',
  company: '',
  email: '',
  phone: '',
  message: ''
}

class Callback extends Component {

  state = { ...defaultState }

  async onSubmit(e){
    e.preventDefault();

    try {
      this.validator();

      let mail = contact(
        this.state.name,
        this.state.company,
        this.state.phone,
        this.state.email,
        this.state.message
      );
      await sendMessage.createMail(this.state.email, mail);

      NotificationManager.success(translate.success);
      this.setState({ ...defaultState });
    } catch (e) {
      try{
        // eslint-disable-next-line
        e = JSON.parse(e)
      } catch (e) {}
      NotificationManager.error(e.Message || e.message || e);
    }
  }

  validator() {
    if (this.state.name === '') throw new Error(translate.fullnameReq);
    if (this.state.email === '') throw new Error(translate.emailReq);

    if (!numbersRegex.test(this.state.name)) {
      throw new Error(translate.fullnameInvalid);
    }

    if (!emailRegex.test(this.state.email)) {
      throw new Error(translate.emailInvalid);
    }

    if (this.state.phone !== '' && !phoneRegex.test(this.state.phone)) {
      throw new Error(translate.phoneInvalid);
    }
  }

  render() {
    return (
      <div className={css(styles.wrapper)} id="callback">
        <CallbackModal text={translate.callbakck} />
        <span className={css(styles.or)}>{translate.or}</span>
        <h4>{translate.contactus}</h4>
        <p>Bos en Lommerplein 270, 1055 RW, Amsterdam<br />contact@arbi.io | Tel: +31 6 2936 9656</p>
        
        <form onSubmit={e => this.onSubmit(e)} className={css(styles.form)}>
          <input
            value={this.state.name}
            onChange={e => this.setState({ name: e.target.value })}
            placeholder="Name *"
            className={css(styles.input)} />
          <input
            value={this.state.email}
            onChange={e => this.setState({ email: e.target.value })}
            placeholder="Email *"
            className={css(styles.input)} />
          <input
            value={this.state.phone}
            onChange={e => this.setState({ phone: e.target.value })}
            placeholder="Phone"
            className={css(styles.input)} />
          <input
            value={this.state.company}
            onChange={e => this.setState({ company: e.target.value })}
            placeholder="Company"
            className={css(styles.input)} />
          <textarea
            value={this.state.message}
            onChange={e => this.setState({ message: e.target.value })}
            placeholder="Message"
            className={css(styles.input)} />
          <div className={css(styles.button)}>
            <button>Send</button>
          </div>
        </form>
      </div>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    contactus: 'Contact Us',
    callbakck: 'Call me back please!',
    success: 'Thank you for your message. We will reply within 24 hours or less. Promised!',
    fullnameReq: 'Fullname is required field.',
    companyReq: 'Company is required field.',
    phoneReq: 'Phone is required field.',
    emailReq: 'Email is required field.',
    fullnameInvalid: 'Full name field is invalid.',
    phoneInvalid: 'Email field is invalid.',
    emailInvalid: 'Phone number field is invalid.',
    or: 'or'
  },
  nl: {
    contactus: 'Neem contact met ons op.',
    callbakck: 'Bel mij aub terug!',
    success: 'Bedankt voor je bericht. We antwoorden binnen 24 uur of minder. Beloofd!',
    fullnameReq: 'Volledige naam is verplicht veld.',
    companyReq: 'Bedrijf is verplicht veld.',
    phoneReq: 'Telefoon is verplicht veld.',
    emailReq: 'E-mail is verplicht veld.',
    fullnameInvalid: 'Het veld Volledige naam is ongeldig.',
    phoneInvalid: 'Het e-mailveld is ongeldig.',
    emailInvalid: 'Telefoonnummer veld is ongeldig.',
    or: 'of'
  }
})

const styles = StyleSheet.create({
  wrapper: {
    background: '#f05272',
    padding: 50,
    display: 'block',
    textAlign: 'center',
    zIndex: 1,
    position: 'relative',
    color: '#000',
    ':nth-child(1n) > h4': {
      letterSpacing: '0.15em',
      fontSize: 45,
      // paddingTop: 50,
      // paddingBottom: 20,
      marginTop: 20,
      fontWeight: 'bold',
      '@media (max-width: 720px)': {
        fontSize: 30,
      },
      '@media (max-width: 470px)': {
        fontSize: 25,
      }
    },
    ':nth-child(1n) > p': {
      fontSize: 16,
      fontWeight: 100
    },
    '@media (max-width: 490px)': {
      padding: '50px 15px'
    }
  },

  form: {
    width: 475,
    display: 'flex',
    flexDirection: 'column',
    margin: '35px auto',
    '@media (max-width: 580px)': {
      width: 'calc(100% - 30px)',
      margin: '35px 15px',
    }
  },

  input: {
    margin: 0,
    border: 0,
    fontSize: 14,
    color: '#000',
    background: '#fff',
    overflow: 'hidden',
    marginTop: 15,
    padding: 7,
    maxWidth: '100%',
    minWidth: '100%',
    boxSizing: 'border-box',
    borderRadius: 0,
    ':focus': {
      outline: 0,
      border: 0,
    },
    ':active': {
      outline: 0,
      border: 0,
    }
  },

  button: {
    display: 'block',
    height: 20,
    background: '#fff',
    padding: 5,
    ':nth-child(1n) > button': {
      background: '#000',
      color: '#fff',
      padding: '15px 40px',
      border: 0,
      fontWeight: 100,
      cursor: 'pointer',
      fontSize: 14,
      transition: '0.5s ease',
      ':hover': {
        background: '#333',
        transition: '0.5s ease'
      }
    }
  },

  or: {
    textAlign: 'center',
    color: '#333',
    display: 'block',
    marginTop: 30,
    fontSize: 20
  }
})

export default connect(state => ({ lang: state.lang }))(Callback)