import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite'
// import { withRouter } from 'react-router'
import scrollToElement from 'scroll-to-element'
import MultiLanguage from 'react-redux-multilang'
import { connect } from 'react-redux'

import right1 from '../../../sources/img/right1.jpeg'
import right2 from '../../../sources/img/right2.jpeg'
import lefttop1 from '../../../sources/img/lefttop1.jpg'
import lefttop2 from '../../../sources/img/lefttop2.jpg'
import leftbot1 from '../../../sources/img/leftbot1.jpeg'
import leftbot2 from '../../../sources/img/leftbot2.jpeg'
import leftbot3 from '../../../sources/img/leftbot3.jpeg'
import leftbot4 from '../../../sources/img/leftbot4.jpeg'

import { buttons } from '../style'
import CallbackModal from './callbackModal'

class Collage extends Component{

  render(){
    return (
      <div className={css(styles.wrapper)}>
        <div className={css(styles.leftBox)}>
          <div>
            <div style={{ background: 'url('+lefttop1+')' }}></div>
            <div style={{ background: 'url('+lefttop2+')' }}></div>
          </div>
          <div>
            <div style={{ background: 'url('+leftbot1+')' }}></div>
            <div style={{ background: 'url('+leftbot2+')' }}></div>
            <div style={{ background: 'url('+leftbot3+')' }}></div>
            <div style={{ background: 'url('+leftbot4+')' }}></div>
          </div>
        </div>
        <div className={css(styles.rightBox)}>
          <div style={{ background: 'url('+right1+')' }}></div>
          <div style={{ background: 'url('+right2+')' }}></div>
        </div>

        <div className={css(styles.info)}>
          <h4>{translate.title}</h4>
          <p>{translate.desc}</p>
          <div className={css(styles.line)}></div>
          <div className={css(styles.buttons)}>
            <button onClick={() => scrollToElement('#callback', { offset: -80 })} className={css(buttons.green, buttons.yellow)}>{translate.getstart}</button>
            <span>{translate.or}</span>
            <CallbackModal text={translate.callback} />
          </div>
        </div>
      </div>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    callback: 'Call me back please!',
    getstart: 'Let\'s get started!',
    or: 'or',
    title: 'EVERY RESTAURANT HAS A STORY TO TELL.',
    desc: 'Tell yours with our 3D Food Menu using Augmented Reality.',
    
  },
  nl: {
    callback: 'Bel mij aub terug!',
    getstart: 'Ik wil graag starten!',
    or: 'of',
    title: 'ACHTER ELKE CULINAIRE CREATIE ZIT EEN UNIEK VERHAAL.',
    desc: 'Vertel jouw verhaal met ons 3D Food Menu door middel van Augmented Reality.',
  }
})

const styles = StyleSheet.create({
  wrapper: {
    paddingTop: 100,
    display: 'flex',
    position: 'relative',
    background: '#fff',
  },

  leftBox: {
    flex: 6,
    ':nth-child(1n) > div': {
      display: 'flex',
      flex: 1,
      ':nth-child(1n) > div:first-child': {
        marginBottom: 10
      },
      ':nth-child(1n) > div': {
        marginRight: 10,
        height: 300,
        width: '100%',
        display: 'block',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        transition: '0.6s ease',
        position: 'relative',
        ':hover': {
          ':after': {
            background: 'rgba(0,0,0,0.5)',
            transition: '0.6s ease'
          }
        },
        ':after': {
          content: '""',
          display: 'block',
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          background: 'rgba(0,0,0,0)',
          transition: '0.6s ease'
        }
      }
    },
    ':nth-child(1n) > div:last-child': {
      ':nth-child(1n) > div': {
        flex: 1
      },
      ':nth-child(1n) > div:nth-child(1)': {
        flex: 2
      },
      ':nth-child(1n) > div:nth-child(2)': {
        flex: 2
      },
      ':nth-child(1n) > div:nth-child(3)': {
        flex: 3
      }
    }
  },

  rightBox: {
    flex: 3,
    display: 'flex',
    flexDirection: 'column',
    overflow: 'hidden',
    ':nth-child(1n) > div:first-child': {
      marginBottom: 10
    },
    ':nth-child(1n) > div': {
      minHeight: 300,
      display: 'block',
      backgroundSize: 'cover',
      backgroundPosition: 'center',
      transition: '0.6s ease',
        position: 'relative',
        ':hover': {
          ':after': {
            background: 'rgba(0,0,0,0.5)',
            transition: '0.6s ease'
          }
        },
        ':after': {
          content: '""',
          display: 'block',
          position: 'absolute',
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          background: 'rgba(0,0,0,0)',
          transition: '0.6s ease'
        }
    }
  },

  info: {
    position: 'absolute',
    width: 450,
    background: '#fff',
    padding: 25,
    left: 'calc(50% - 250px)',
    top: 250,
    textAlign: 'center',
    boxSizing: 'border-box',
    ':nth-child(1n) > h4': {
      fontSize: 24,
      fontWeight: 'bold',
      fontFamily: 'verdana,geneva,sans-serif'
    },
    ':nth-child(1n) > p': {
      fontSize: 18
    },
    '@media (max-width: 550px)': {
      width: 'calc(100% - 30px)',
      left: 'initial',
      margin: '0 15px'
    }
  },

  line: {
    display: 'block',
    width: 100,
    borderTop: '1px solid #ccc',
    margin: '25px auto',
  },

  buttons: {
    display: 'flex',
    justifyContent: 'space-between',
    ':nth-child(1n) > span': {
      fontWeight: 'bold',
      paddingTop: 5,
      '@media (max-width: 480px)': {
        padding: '15px 0'
      }
    },
    '@media (max-width: 480px)': {
      flexDirection: 'column',
    }
  }

})

export default connect(state => ({ lang: state.lang }))(Collage)