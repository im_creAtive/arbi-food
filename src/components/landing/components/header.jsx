import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite'
import { withRouter } from 'react-router'
import MultiLanguage, { setLanguage } from 'react-redux-multilang'
import { connect } from 'react-redux'

import foodStory from '../../../sources/img/foodstory.png'

import { buttons } from '../style'
import CallbackModal from './callbackModal'

class Header extends Component{

  onSelectLanguage(code){
    setLanguage(code)
    localStorage.setItem('language', code)
  }

  render(){
    return (
      <header className={css(styles.header)}>
        <div>
          <div className={css(styles.buttonDiv)}>
            <CallbackModal text={translate.callback} />
          </div>
          <a href="/"><img src={foodStory} alt="logo" /></a>
          <div className={css(styles.buttonDiv)}>
            <button className={css(buttons.green, buttons.yellow, styles.login)} onClick={() => this.props.history.push('/login')}>
              {translate.login}<i className="material-icons">account_box</i>
            </button>
          </div>
        </div>
      </header>
    )
  }

}

const translate = new MultiLanguage({
  en: {
    callback: 'Call me back please!',
    login: 'Login'
  },
  nl: {
    callback: 'Bel mij aub terug!',
    login: 'Login'
  }
})

const styles = StyleSheet.create({
  header: {
    overflow: 'auto',
    position: 'fixed',
    width: '100%',
    zIndex: 5,
    background: '#fff',
    borderBottom: '2px solid #f05273',
    ':nth-child(1n) > div': {
      width: 980,
      margin: '15px auto',
      display: 'flex',
      justifyContent: 'space-around',
      '@media (max-width: 980px)': {
        width: '100%'
      },
      ':nth-child(1n) > a > img': {
        '@media (max-width: 450px)': {
          height: 25,
          marginTop: 5
        }
      }
    }
  },

  buttonDiv: {
    justifyContent: 'center',
    display: 'flex',
    flexDirection: 'column',
    ':nth-child(1n) > button': {
      '@media (max-width: 580px)': {
        minWidth: 'initial'
      }
    }
  },

  login: {
    ':nth-child(1n) > i': {
      display: 'none',
      '@media (max-width: 500px)': {
        display: 'inline',
        fontSize: 17
      }
    },
    '@media (max-width: 500px)': {
      fontSize: 0
    }
  },

})

export default connect(state => ({ lang: state.lang }))(withRouter(Header))