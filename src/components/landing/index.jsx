import React, { Component } from 'react'
import { css, StyleSheet } from 'aphrodite/no-important'
// import { toast } from 'react-toastify'
import './style.css'

import videoMp4 from '../../sources/img/bg.mp4'
import videoWebm from '../../sources/img/bg.webm'
import noVideo from '../../sources/img/lefttop1.jpg'

// import * as loginActions from '../../actions/login'

import Header from './components/header'
import Collage from './components/collage'
import Why from './components/why'
import Video from './components/video'
import Story from './components/story'
import Slider from './components/slider'
import Mobile from './components/mobile'
import Callback from './components/callback'
import Faq from './components/faq'
import Footer from './components/footer'
import Language from './components/language'

const defState = {
  
}

class Landing extends Component{

  state = { ...defState }

  render(){
    return (
      <div id="landing" className={css(styles.wrapper)}>
        <div className={css(styles.bg)}>
          <video loop autoPlay playsInline muted>
            <source type="video/mp4" src={videoMp4} />
            <source type="video/webm" src={videoWebm} />
            <img src={noVideo} alt="no-video" />
          </video>
        </div>
        <Language />
        <Header />
        <Collage />
        <Why />
        <Video />
        <Story />
        <Slider />
        <Mobile />
        <Callback />
        <Faq />
        <Footer />
      </div>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    overflow: 'auto'
  },

  bg: {
    display: 'block',
    position: 'fixed',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    width: '100%',
    ':nth-child(1n) > video': {
      width: '100%',
      '@media (max-width: 840px)': {
        width: 'initial',
        height: '100vh',
        minWidth: '100%',
        margin: '0 calc(100% - 1060px)',
      },
      '@media (max-width: 680px)': {
        margin: '0 calc(100% - 840px)',
      }
    }
  }
})



export default Landing