import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import getMuiTheme from 'material-ui/styles/getMuiTheme'


let theme = Object.assign(lightBaseTheme);

// transform
theme.palette.primary1Color = '#57386e';
theme.palette.accent1Color = '#588b8b';

export default getMuiTheme(theme);