import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';
import { syncWithStore, setLanguage } from 'react-redux-multilang'

// redux & router
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import createHistory from 'history/createBrowserHistory';
import { ConnectedRouter, routerMiddleware } from 'react-router-redux';
import reducers from './reducers';

// styles
import { ToastContainer } from 'react-toastify';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import theme from './sources/theme';
import '../node_modules/material-design-icons/iconfont/material-icons.css';
import './sources/index.css';

// components
import Wrapper from './components/wrapper'
import LoadingScreen from './components/wrapper/loadingScreen'

export const history = createHistory()
const middleware = routerMiddleware(history)

export const store = createStore(reducers, applyMiddleware(middleware))
// language settings
let language = localStorage.getItem('language');
if(!language) language = navigator.language || navigator.userLanguage
if(!language || ['en','nl'].indexOf(language) < 0) language = 'en'
syncWithStore(store);
setLanguage(language);

ReactDOM.render(
    <MuiThemeProvider muiTheme={theme}>
        <Provider store={store}>
            <ConnectedRouter history={history}>
                <div id="container">
                    <LoadingScreen />
                    <ToastContainer />
                    <Wrapper />
                </div>
            </ConnectedRouter>
        </Provider>
    </MuiThemeProvider>,
    document.getElementById('root')
)
registerServiceWorker();
